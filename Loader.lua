----------------------------------------------------------------------------------------
--                                   EISS 2.0                                         --
-- 								 Codename "Adonis"                                    --
----------------------------------------------------------------------------------------
--		   	  Epix Incorporated. Not Everything is so Black and White.		   		  --
----------------------------------------------------------------------------------------
--	    Edit settings in-game or using the settings module in the Config folder	      --
----------------------------------------------------------------------------------------

if _G["__Adonis_MUTEX"] and type(_G["__Adonis_MUTEX"])=="string" then
	warn("Adonis is already running! Aborting...")
	warn("Running Loader's Location: ".._G["__Adonis_MUTEX"])
	warn("This Loader's Location: "..script:GetFullName())
	error("An existing instance of Adonis  is already running! Read above for more info")
else
	_G["__Adonis_MUTEX"] = script:GetFullName()
	
	if #game:service("Players"):GetChildren()>0 then warn("WARNING: Adonis is NOT designed to work in solo mode!") end
	
	local moduleId = 359948692
	
	local model = script.Parent.Parent
	local config = model.Config
	local core = model.Loader
	
	local dropper = core.Dropper
	local loader = core.Loader
	
	local settings = config.Settings
	local plugins = config.Plugins
	local themes = config.Themes
	
	
	local data = {
		Settings = {};
		ServerPlugins = {};
		ClientPlugins = {};
		Themes = {};
		
		Model = model;
		Config = config;
		Core = core;
		
		Loader = loader;
		Dopper = dropper;
		
		DebugMode = true;
	}
	
	-- //Init
	
	script.Parent = nil
	data.Settings = require(settings)
	for _,Plugin in next,plugins:GetChildren() do if Plugin.Name:sub(1,8)=="Client: " then table.insert(data.ClientPlugins,Plugin) elseif Plugin.Name:sub(1,8)=="Server: " then table.insert(data.ServerPlugins,Plugin) else warn("Unknown Plugin Type for "..tostring(Plugin)) end end
	for _,Theme in next,themes:GetChildren() do table.insert(data.Themes,Theme) end
	if data.DebugMode then moduleId = model.Parent.MainModule end
	local module = require(moduleId)
	local response = module(data)
	if response == "SUCCESS" then
		model.Parent = nil
		game.Close:connect(function() model.Parent = game:service("ServerScriptService") end)
	else
		error("MainModule failed to load")
	end
end

																																																							--[[
--_____________________________________________________________________________________________________________________--
--_____________________________________________________________________________________________________________________--																					
--_____________________________________________________________________________________________________________________--
--_____________________________________________________________________________________________________________________--																						
--																					                                   --	
		
								___________      .__         .___                   
								\_   _____/_____ |__|__  ___ |   | ____   ____      
								 |    __)_\____ \|  \  \/  / |   |/    \_/ ___\     
								 |        \  |_> >  |>    <  |   |   |  \  \___     
								/_______  /   __/|__/__/\_ \ |___|___|  /\___  > /\ 
								        \/|__|            \/          \/     \/  \/
							  --------------------------------------------------------
							  Epix Incorporated. Not Everything is so Black and White.
							  --------------------------------------------------------
						
						
--_____________________________________________________________________________________________________________________--
--_____________________________________________________________________________________________________________________--																				
--_____________________________________________________________________________________________________________________--
--_____________________________________________________________________________________________________________________--
--																					                                   --	
																																																							--]]






