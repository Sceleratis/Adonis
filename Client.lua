-------------------
-- Adonis Client --
-------------------

local folder = script:WaitForChild("Deps")
local depsFolder = folder:Clone()
repeat wait(1/60) script.Parent = nil until script.Parent == nil
local clientDeps = {}
for ind,obj in pairs(depsFolder:GetChildren()) do clientDeps[obj.Name] = obj obj.Parent = nil end
folder:Destroy()

--// I have my reasons...

local _G = _G
local script = script
local game = game
local workspace = workspace
local getfenv = getfenv
local setfenv = setfenv
local getmetatable = getmetatable
local setmetatable = setmetatable
local math = math
local print = print
local warn = warn
local error = error
local coroutine = coroutine
local pcall = pcall
local ypcall = ypcall
local xpcall = xpcall
local select = select
local rawset = rawset
local rawget = rawget
local rawequal = rawequal
local pairs = pairs
local ipairs = ipairs
local next = next
local newproxy = newproxy
local os = os
local tick = tick
local loadstring = loadstring
local tostring = tostring
local tonumber = tonumber
local unpack = unpack
local string = string
local Instance = Instance
local type = type
local wait = wait
local require = require
local table = table
local Enum = Enum
local origEnv = getfenv()

setfenv(1,{})

--// Important variables

local client = {}
local function logError(plr,error) warn(error) client.Remote.Send("LogError",error) end
local print = function(...) for i,v in pairs({...}) do print(':: Adonis :: '..tostring(v)) end  end
local warn = function(...) for i,v in pairs({...}) do warn(':: Adonis :: '..tostring(v)) end end
local cPcall = function(func,...) local function cour(...) coroutine.resume(coroutine.create(func),...) end local ran,error=ypcall(cour,...) if error then logError(error) warn('ERROR :: '..error) end end
local Pcall = function(func,...) local ran,error=ypcall(func,...) if error then logError(error) warn('ERROR :: '..error) end end
local Routine = function(func,...) coroutine.resume(coroutine.create(func),...) end
local service; service = setmetatable({
		MarketPlace = game:service("MarketplaceService");
		GamepassService = game:service("GamePassService");
		ChatService = game:service("Chat");
		Player = game:service("Players").LocalPlayer;
		PlayerGui = game:service("Players").LocalPlayer.PlayerGui;
		LocalContainer=function() if not client.Variables.localContainer or not client.Variables.localContainer.Parent then client.Variables.localContainer=Instance.new("Camera",workspace) client.Variables.localContainer.Name=math.random(10000000,999999999) end return client.Variables.localContainer end;
	},{
	__index = function(tab,index)
		local serv
		local ran,err = ypcall(function() serv = game:GetService(index) end)
		if ran then
			return serv
		end
	end
})


--// Setting things up

client = {
	Service = service;
	Print = print;
	Warn = warn;
	cPcall = cPcall;
	Pcall = Pcall;
	Routine = Routine;
	LogError = logError;
	Deps = clientDeps;
	
	GUIs = {};
	
	Loops = {};
	
	Variables = {
		KeyBinds = {};
		savedUI = {};
		localSounds = {};
	};
	
	Disconnect = function()
		service.Player:Kick()
		wait(30)
		client.Kill()
	end;
	
	Kill = function()
		while true do 
			client.Crash() 
		end
	end;
	
	
	--// Remote/core stuff
	Remote = {
		Returns = {};
		
		Returnables = {
			Test = function(args)
				return "HELLO FROM THE CLIENT SIDE :)! "..tostring(args[1])
			end;
			
			Ping = function(args)
				return client.Remote.Ping()
			end;
			
			ClientHooked = function(args)
				return script.Name
			end;
			
			UIKeepAlive = function(args)
				print("KEEPING IT ALIVE")
				for i,v in pairs(client.Variables.savedUI) do
					print(v)
					local existing = service.PlayerGui:FindFirstChild(v.Name)
					v.Parent = service.PlayerGui
					if existing then
						existing:Destroy()
					end
				end
				return "Done"
			end;
			
			UI = function(args)
				local guiName = args[1]
				local themeData = args[2]
				local guiData = args[3]
				
				client.Core.Theme = themeData
				return client.Core.MakeGui(guiName,guiData,themeData)
			end;
		};
		
		UnEncrypted = {
			LightingChange = function(args)
				print(args[1])
				print("TICKLE ME!?")
				client.Variables.LightingChanged = true
				service.Lighting[args[1]] = args[2]
				client.Anti.LastChanges.Lighting = args[1]
				wait(0.1)
				client.Variables.LightingChanged = false
				print("TICKLED :)")
				print(client.Variables.LightingChanged)
				if client.Anti.LastChanges.Lighting == args[1] then
					client.Anti.LastChanges.Lighting = nil
				end
			end;
		};
		
		Commands = {
			GetReturn = function(args)
				local com = args[1]
				local key = args[2]
				local parms = {args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10],args[11],args[12]} -- unpack() issues..
				local retable = client.Remote.Returnables[com]
				local ret
				if retable then
					ret = retable(parms)
				else
					ret = nil
				end
				client.Remote.Send("GiveReturn",key,ret)
			end;
			
			GiveReturn = function(args)
				local key = args[1]
				client.Remote.Returns[key] = {args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10],args[11]}
			end;
			
			Test = function(args)
				print("OK WE GOT COMMUNICATION!  ORGL: "..args[1])
			end;
			
			LaunchAnti = function(args)
				client.Anti.Launch(args[1],args[2])
			end;
			
			Function = function(args)
				if client.Functions[args[1]] then
					client.Functions[args[1]](args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10],args[11])
				end
			end;
			
			UI = function(args)
				local guiName = args[1]
				local themeData = args[2]
				local guiData = args[3]
				
				client.Core.Theme = themeData
				client.Core.MakeGui(guiName,guiData,themeData)
			end;
			
			Function = function(args)
				local func = client.Functions[args[1]]
				if func and type(func)=="function" then
					Pcall(func,args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10],args[11])
				end
			end;
		};
		
		Fire = function(...)
			if client.Core.RemoteEvent and client.Core.RemoteEvent.Object then
				client.Core.RemoteEvent.Object:FireServer(...)
			end
		end;
		
		Send = function(com,...)
			client.Remote.Fire(client.Remote.Encrypt(com,client.Core.Key),...)
		end;
		
		Get = function(com,...)
			local key = client.Functions.GetRandom()
			client.Remote.Returns[key] = key
			local t = os.time()
			client.Remote.Send("GetReturn",com,key,...)
			repeat service.RunService.RenderStepped:wait() until client.Remote.Returns[key] ~= key or os.time()-t>300
			local args = client.Remote.Returns[key]
			if args == key or not args then
				return nil
			elseif type(args)=="table" then
				return args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10]	
			end
		end;
		
		Ping = function()
			local t=tick()
			client.Remote.Get("Ping")
			local t2=tick()
			local mult = 10^3
			local ms=((math.floor((t2-t)*mult+0.5)/mult)*100)
			return ms
		end;
		
		Encrypt = function(str,key)
			local keyBytes = {}
			local strBytes = {}
			local endStr = ""
			for i=1,#key do table.insert(keyBytes,string.byte(key:sub(i,i))) end 
			for i=1,#str do table.insert(strBytes,string.byte(str:sub(i,i))) end 
			for i=1,#strBytes do
				if i%#keyBytes>0 then
					if strBytes[i]+keyBytes[i%#keyBytes]>255 then
						strBytes[i]=math.abs(strBytes[i]-keyBytes[i%#keyBytes])
					else
						strBytes[i]=math.abs(strBytes[i]+keyBytes[i%#keyBytes])
					end
				else
					if strBytes[i]+keyBytes[1]>255 then
						strBytes[i]=math.abs(strBytes[i]-keyBytes[1])
					else
						strBytes[i]=math.abs(strBytes[i]+keyBytes[1])
					end
				end
			end
			for i=1,#strBytes do endStr=endStr..string.char(strBytes[i]) end
			return endStr
		end;
		
		Decrypt = function(str,key)
			local keyBytes = {}
			local strBytes = {}
			local endStr = ""
			for i=1,#key do table.insert(keyBytes,string.byte(key:sub(i,i))) end 
			for i=1,#str do table.insert(strBytes,string.byte(str:sub(i,i))) end 
			for i=1,#strBytes do
				if i%#keyBytes>0 then
					if strBytes[i]+keyBytes[i%#keyBytes]>255 then
						strBytes[i]=math.abs(strBytes[i]-keyBytes[i%#keyBytes])
					else
						strBytes[i]=math.abs(keyBytes[i%#keyBytes]-strBytes[i])
					end
				else
					if strBytes[i]+keyBytes[1]>255 then
						strBytes[i]=math.abs(strBytes[i]-keyBytes[1])
					else
						strBytes[i]=math.abs(keyBytes[1]-strBytes[i])
					end
				end
			end
			for i=1,#strBytes do endStr=endStr..string.char(strBytes[i]) end
			return endStr
		end;
	};
	
	--// Core
	Core = {
		Name = script.Name;
		
		GetEvent = function()
			if client.Core.RemoteEvent then
				client.Core.RemoteEvent.Event:disconnect()
				client.Core.RemoteEvent.Security:disconnect()
				client.Core.RemoteEvent = nil
			end
			client.Core.RemoteEvent = {}
			local event
			local t = os.time()
			repeat event = service.ReplicatedStorage:FindFirstChild(client.Core.Name) wait() until event or os.time()-t>300
			if event then
				client.Core.RemoteEvent.Object = event
				client.Core.RemoteEvent.Event = event.OnClientEvent:connect(client.Process.Remote)
				client.Core.RemoteEvent.Security = event.Changed:connect(function(p)
					if p=="RobloxLocked" and client.Anti.ObjRLocked(event) then
						if not client.Variables.IsAdmin then
							client.Core.Kill()
						end
					end
				end)
			else
				client.Core.Kill()
			end
		end;
		
		Init = function()
			pcall(function() service.StarterPlayer:FindFirstChild(client.Core.Name):Destroy() end)
			
			client.Core.GetEvent()
			client.Remote.Fire(client.Core.Name.."GET_KEY")
			repeat wait() until client.Core.Key
			
			service.ReplicatedStorage.Changed:connect(function(p)
				if client.Functions.RLocked(service.ReplicatedStorage) then
					if not client.Variables.IsAdmin then 
						client.Core.Kill()
					end
				end
			end)
			
			service.ReplicatedStorage.ChildRemoved:connect(function(c)
				if client.Core.RemoteEvent and c==client.Core.RemoteEvent.Object then
					client.Core.GetEvent()
					client.Core.CheckClient()
				end
			end)
			
			service.ReplicatedStorage.ChildRemoved:connect(function(c)
				if c==client.Core.RemoteEvent.Object then 
					client.Core.GetEvent()
				end
			end)
			
			client.Variables.CodeName = client.Remote.Get("Variable","CodeName")
			
			client.Core.Loadstring = require(client.Deps.Loadstring)
			for index,plugin in pairs(client.Deps.Plugins:GetChildren()) do client.Core.LoadPlugin(plugin) end
			
			service.Players.LocalPlayer.Chatted:connect(client.Process.Chat)
			service.Player.CharacterRemoving:connect(client.Process.CharacterRemoving) 
		end;
		
		GetEnv = function(oldenv)
			local env = setmetatable({},{
				__index = function(tab,ind)
					local e = oldenv
					if e[ind] then
						return e[ind]
					end
				end
			})
			
			env.print = print
			env.warn = warn
			
			env.client = client
			env.service = service
			
			env.cPcall = cPcall
			env.Pcall = Pcall
			env.Routine = Routine
			
			env.logError = logError
			
			return env
		end;
		
		LoadPlugin = function(plugin)
			local plug = require(plugin)
			local func = setfenv(plug,client.Core.GetEnv(getfenv(plug)))
			cPcall(func)
		end;
		
		MakeGui = function(guiName,guiData,themeData)
			local themeData = themeData or client.Core.Theme
			local theme = themeData.Desktop
			local folder = client.Deps.UI:FindFirstChild(theme)
			
			if folder then
				local gui = folder:FindFirstChild(guiName)
				if gui then
					local allowMult = gui.Config.AllowMultiple.Value
					local found, num = client.Core.GetGui(gui.Name)
					if not ((num and num>0) and allowMult) then
						local new = gui:Clone()
						local config = new.Config
						local code = new.Config.Code
						local env = client.Core.GetEnv(origEnv)
						local data = guiData or {}
						local gIndex = client.Functions.GetRandom()
						local gTable = {Object = new, Name = new.Name}
						
						client.GUIs[gIndex] = gTable
						new.Changed:connect(function(p)
							if p=="Parent" and new.Parent==nil then
								client.GUIs[gIndex] = nil
							end
						end)
						
						new.Name = gIndex
						env.script = code 
						data.gIndex = gIndex
						data.gTable = gTable
						
						return setfenv(require(code),env)(data)
					end
				else
					warn(tostring(guiName).." not found in ")
				end
			else
				warn(tostring(theme).." not found")
			end
		end;
		
		GetGui = function(obj,ignore)
			local found = {}
			local num = 0
			if obj then
				for ind,g in pairs(client.GUIs) do
					if g.Name~=ignore and g.Object~=ignore then
						if type(obj)=="string" then
							if g.Name==obj then
								found[ind] = g
								num = num+1
							end
						elseif type(obj)=="userdata" then
							if g.Object==obj then
								found[ind] = g
								num = num+1
							end
						end
					end
				end
			end
			if num<1 then 
				return false
			else
				return found, num 
			end
		end;
	};
	
	
	--// Processing
	Process = {
		Remote = function(com,...)
			local args = {...}
			if type(com)=="string" then
				if com=="GIVE_KEY" then
					client.Core.Key = args[1]
				elseif client.Remote.UnEncrypted[com] then
					Pcall(client.Remote.UnEncrypted[com],args)
				elseif client.Core.Key then
					local command = client.Remote.Commands[client.Remote.Decrypt(com,client.Core.Key)]
					if command then 
						Pcall(command,args)
					end
				end
			end
		end;
		
		Chat = function(msg)
			client.Remote.Send("ProcessChat",msg)
		end;
		
		CharacterRemoving = function()
			client.Variables.savedUI = {}
			for i,v in pairs(service.PlayerGui:children()) do
				Routine(function()
					client.Variables.savedUI[v.Name] = v
					if v.Name:sub(1,#client.CodeName) == client.CodeName then
						v.Parent = nil
					else
						client.Variables.savedUI[v.Name] = nil
					end
				end)
			end
			local textbox = service.UserInputService:GetFocusedTextBox()
			if textbox then 
				pcall(function() textbox:ReleaseFocus() end)
			end
		end;
	};
	
	
	--// Function stuff
	Functions = {
		GetRandom = function()
			local str = ""
			for i=1,math.random(5,10) do str=str..string.char(math.random(1,255)) end
			return str
		end;
		
		KeyBindListener = function()
			local timer = 0
			client.Variables.KeyBinds = client.Remote.Get("PlayerData").Keybinds or {}
				
			service.UserInputService.InputBegan:connect(function(input)
				local key = tostring(input.KeyCode.Value)
				local textbox = service.UserInputService:GetFocusedTextBox()
				
				if not (textbox) and key and client.Variables.KeyBinds[key] and not client.Variables.WaitingForBind then 
					local isAdmin = client.Remote.Get("CheckAdmin")
					if tick() - timer>5 or isAdmin then
						client.Remote.Send('ProcessCommand',client.Variables.KeyBinds[key],false,true)
						client.Core.MakeGui("Hint",{
							Message = "[Ran] Key: "..string.char(key).." | Command: "..client.Variables.KeyBinds[key]
						})
					end
					timer = tick()
				end
			end)
		end;
		
		AddKeyBind = function(key,command)
			client.Variables.KeyBinds[tostring(key)] = command
			client.Remote.Get("UpdateKeybinds",client.Variables.KeyBinds)
			client.Core.MakeGui("Hint",{
				Message = 'Bound "'..string.char(key)..'" to '..command
			})
		end;
		
		RemoveKeyBind=function(key)
			if client.Variables.KeyBinds[tostring(key)] ~= nil then
				client.Variables.KeyBinds[tostring(key)] = nil
				client.Remote.Get("UpdateKeybinds",client.Variables.KeyBinds)
				client.Core.MakeGui("Hint",{
					Message = 'Removed "'..string.char(key)..'" from key binds'
				})
			end
		end;
		
		PlayAudio = function(audioId,volume,pitch,looped)
			if client.Variables.localSounds[tostring(audioId)] then client.Variables.localSounds[tostring(audioId)]:Stop() client.Variables.localSounds[tostring(audioId)]:Destroy() client.Variables.localSounds[tostring(audioId)]=nil end
			local sound=Instance.new("Sound")
			sound.SoundId="rbxassetid://"..audioId
			if looped then sound.Looped=true end
			if volume then sound.Volume=volume end
			if pitch then sound.Pitch=pitch end
			sound.Name="[EISS] LOCAL SOUND "..audioId
			sound.Parent=service.LocalContainer()	
			client.Variables.localSounds[tostring(audioId)]=sound
			sound:Play()
			wait(1)
			repeat wait(0.1) until not sound.IsPlaying
			sound:Destroy()
			client.Variables.localSounds[tostring(audioId)]=nil
		end;
		
		StopAudio = function(audioId)
			if client.Variables.localSounds[tostring(audioId)] then 
				client.Variables.localSounds[tostring(audioId)]:Stop() 
				client.Variables.localSounds[tostring(audioId)]:Destroy() 
				client.Variables.localSounds[tostring(audioId)]=nil 
			end
		end;
		
		KillAllLocalAudio = function()
			for i,v in pairs(client.Variables.localSounds) do
				v:Stop()
				v:Destroy()
				table.remove(client.Variables.localSounds,i)
			end
		end;
		
		SetFps = function(fps)
			client.Loops.setFps=false
			wait(0.5)
			client.Loops.setFps=true
			while wait(0.1) and client.Loops.setFps do
				local ender = tick()+1/fps
				repeat until tick()>=ender
			end
		end;
		
		RestoreFps = function()
			client.Loops.setFps = false
		end;
		
		Crash = function()
			local crash
			crash = function() 
				while true do 
					repeat ypcall(function() 
						print(game[("%s|"):rep(0xFFFFFFF)]) 
						crash() 
					end) 
					until nil 
				end 
			end
			crash()
		end;
		
		HardCrash=function() --// How to overkill 101
			local crash
			local tab
			local gui=Instance.new("ScreenGui",service.PlayerGui)
			local rem=Instance.new("RemoteEvent")
			crash = function()
				for i=1,50 do
				print("((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)((((**&&#@#$$$$$%%%%:)")
				local f=Instance.new('Frame',gui)
				f.Size=UDim2.new(1,0,1,0)
				spawn(function() table.insert(tab,string.rep(tostring(math.random()),100)) end)	
				rem:FireServer("Hiiiiiiiiiiiiiiii")	
				spawn(function()
					spawn(function()
						spawn(function()
							spawn(function()
								spawn(function()
									print("hi")
									spawn(crash)
								end)
							end)
						end)
					end)
				end)
				--print(game[("%s|"):rep(0xFFFFFFF)])
				end
				tab = {}
			end
			while wait(0.01) do
				for i=1,50000000 do
					cPcall(function() client.GPUCrash() end)
					cPcall(function() crash() end)
					print(1)
				end
			end
		end;
		
		GPUCrash = function()
			local crash
			local gui=Instance.new("ScreenGui",service.PlayerGui)
			crash=function()
				while wait(0.01) do
					for i=1,500000 do
						local f=Instance.new('Frame',gui)
						f.Size=UDim2.new(1,0,1,0)
					end
				end
			end
			crash()
		end;
		
		RemoveGuis = function()
			for i,v in pairs(service.PlayerGui:children()) do
				if not v.Name:match(client.Variables.CodeName) then
					v:Destroy()
				end
			end
		end;
		
		SetCoreGuiEnabled = function(element,enabled)
			service.StarterGui:SetCoreGuiEnabled(element,enabled)
		end;
		
		BlurScreen = function(on,trans)
			local exists = workspace.CurrentCamera:FindFirstChild("EISS_WINDOW_FUNC_BLUR")
			if exists then exists:Destroy() end
			if on then
				pa = Instance.new("Part",service.LocalContainer())
				pa.Name = "WINDOW_BLUR"
				pa.Material = "Neon"
				pa.BrickColor = BrickColor.Black()
				pa.Transparency = trans or 0.5
				pa.CanCollide = false
				pa.Anchored = true
				pa.FormFactor = "Custom"
				pa.Size = Vector3.new(100,100,0)
				cPcall(function()
					while pa and pa.Parent and wait(1/40) do
						pa.CFrame=workspace.CurrentCamera.CoordinateFrame*CFrame.new(0,0,-2.5)*CFrame.Angles(12.6,0,0)
					end
				end)
			else
				for i,v in pairs(workspace.CurrentCamera:children()) do
					if v.Name=="WINDOW_BLUR" then
						v:Destroy() 
					end
				end
			end
		end;

		
		UnCape = function()
			local cape = service.LocalContainer():FindFirstChild("::Adonis::Cape")
			if cape then cape:Destroy() end
		end;
		
		Cape = function(material,color,decal,reflect)
			local torso = service.Player.Character:WaitForChild("Torso")
			local p = Instance.new("Part",service.LocalContainer())
			p.Name = "::Adonis::Cape" 
			p.Anchored = true
			p.Transparency=0.1
			p.Material=material
			p.CanCollide = false 
			p.TopSurface = 0 
			p.BottomSurface = 0 
			if type(color)=="table" then 
				color = Color3.new(color[1],color[2],color[3]) 
			end
			p.BrickColor = BrickColor.new(color) or BrickColor.new("White")
			if reflect then
				p.Reflectance=reflect
			end 
			if decal and decal~=0 then
				local dec = Instance.new("Decal", p) 
				dec.Face = 2 
				dec.Texture = "http://www.roblox.com/asset/?id="..decal 
				dec.Transparency=0 
			end
			p.formFactor = "Custom"
			p.Size = Vector3.new(.2,.2,.2)
			local msh = Instance.new("BlockMesh", p) 
			msh.Scale = Vector3.new(9,17.5,.5)
			wait(0.1)
			p.Anchored=false
			local motor1 = Instance.new("Motor", p)
			motor1.Part0 = p
			motor1.Part1 = torso
			motor1.MaxVelocity = .01
			motor1.C0 = CFrame.new(0,1.75,0)*CFrame.Angles(0,math.rad(90),0)
			motor1.C1 = CFrame.new(0,1,torso.Size.Z/2)*CFrame.Angles(0,math.rad(90),0)--.45
			local wave = false
			repeat wait(1/44)
				local ang = 0.1
				local oldmag = torso.Velocity.magnitude
				local mv = .002
				if wave then ang = ang + ((torso.Velocity.magnitude/10)*.05)+.05 
					wave = false 
				else 
					wave = true 
				end
				ang = ang + math.min(torso.Velocity.magnitude/11, .5)
				motor1.MaxVelocity = math.min((torso.Velocity.magnitude/111), .04) + mv
				motor1.DesiredAngle = -ang
				if motor1.CurrentAngle < -.2 and motor1.DesiredAngle > -.2 then 
					motor1.MaxVelocity = .04 
				end
				
				repeat wait() until motor1.CurrentAngle == motor1.DesiredAngle or math.abs(torso.Velocity.magnitude - oldmag) >=(torso.Velocity.magnitude/10) + 1
				
				if torso.Velocity.magnitude < .1 then 
					wait(.1) 
				end
			until not p or not p.Parent or p.Parent ~= service.LocalContainer() 
		end;

	};
	
	
	--// Anti Exploit
	Anti = {
		LastChanges = {
			Lighting = {};
		};
		
		Detectors = {
			Speed = function(data)
				while wait(1) do
					Pcall(function()
						if select(2,ypcall(workspace.GetRealPhysicsFPS)):match('GetRealPhysicsFPS') then
							if workspace:GetRealPhysicsFPS() > tonumber(data.Speed) then
								client.Anti.Detected('kick','Speed exploiting')
							end
						else
							client.Anti.Detected('kick','Method change detected.')
						end
					end)
				end
			end;
			
			LockLighting = function(data)
				--[[
				local lightSets = client.Remote.Get("Variable","LightingSettings")
				service.Lighting.Changed:connect(function(c) 
					if lightSets[c] ~= nil then 
						service.Lighting[c] = lightSets[c] 
						local data = {}
						data.Property = c
						client.Remote.Send("AddReplication","LChanged",data)
					end 
				end)--]]
				local settings = {
					Ambient = service.Lighting.Ambient;
					Brightness = service.Lighting.Brightness;
					ColorShift_Bottom = service.Lighting.ColorShift_Bottom;
					ColorShift_Top = service.Lighting.ColorShift_Top;
					GlobalShadows = service.Lighting.GlobalShadows;
					OutdoorAmbient = service.Lighting.OutdoorAmbient;
					Outlines = service.Lighting.Outlines;
					ShadowColor = service.Lighting.ShadowColor;
					GeographicLatitude = service.Lighting.GeographicLatitude;
					Name = service.Lighting.Name;
					TimeOfDay = service.Lighting.TimeOfDay;				
					FogColor = service.Lighting.FogColor;
					FogEnd = service.Lighting.FogEnd;
					FogStart = service.Lighting.FogStart;
				}
				
				local checking = false
				
				Routine(function()
					while true do
						if not checking then 
							for i,v in pairs(settings) do
								if service.Lighting[i] ~= nil then
									--print(i)
									settings[i] = service.Lighting[i]
								end
							end
						end
						wait(1)
					end
				end)
				
				client.Variables.LightingChanged = false
				local tempIgnore = false
				local function check(c)
					if client.Variables.LightingChanged then return true end
					local temp = service.Lighting[c]
					if service.Lighting[c] ~= nil and settings[c] ~= nil then
						tempIgnore = true
						service.Lighting[c] = settings[c]
						tempIgnore = false
						wait(0.01)
						if c == client.Anti.LastChanges.Lighting then
							tempIgnore = true
							service.Lighting[c] = temp
							tempIgnore = false
							return true
						else
							return false
						end
					end
					--[[
					if c == client.Anti.LastChanges.Lighting.Property then
						return true
					else
						if client.Anti.LastChanges.Lighting.Value then
							service.Lighting[c] = client.Anti.LastChanges.Lighting.Value 
						end
						wait(0.01)
						if c == client.Anti.LastChanges.Lighting.Property then
							service.Lighting[c] = temp
							return true
						else
							return false
						end
					end--]]
				end
				
				service.Lighting.Changed:connect(function(c)
					checking = true
					if not tempIgnore then
						if check(c) then
							print("SERVER CHANGED IT")
						else
							print("CLIENT CHANGED IT")
						end
					end
					checking = false
				end)
			end;
			
			ReplicationLogs = function()
				local function checkReplicationParent(obj,class)
					local full=obj:GetFullName()
					local prev=game
					for s in full:gmatch("[^%.]+") do
						local new=prev:FindFirstChild(s)
						if new then
							if new:IsA(class) or (new.Name:find("[EISS]") and new:IsA("LocalScript")) then
								return true
							else
								prev=new
							end
						else 
							return false
						end
					end
					return false
				end
				
				game.DescendantAdded:connect(function(c)
					if not client.ObjRLocked(c) and not checkReplicationParent(c,"Camera",workspace) and not checkReplicationParent(c,"ScreenGui",workspace) and not checkReplicationParent(c,"InsertService",workspace) then
						--obj,name,class,parent,path,plr
						local data = {}
						data.obj = c
						data.name = c.Name
						data.class = c.ClassName
						data.parent = c.Parent
						data.path = c:GetFullName()
						client.Remote.Fire("AddReplication","Created",data)
					end
				end)
				
				game.DescendantRemoving:connect(function(c)
					if not client.ObjRLocked(c) and not checkReplicationParent(c,"Camera",workspace) and not checkReplicationParent(c,"ScreenGui",workspace) and not checkReplicationParent(c,"InsertService",workspace) then
						local data = {}
						data.obj = c
						data.name = c.Name
						data.class = c.ClassName
						data.parent = c.Parent
						data.path = c:GetFullName()
						client.Remote.Fire("AddReplication","Destroyed",data)
					end
				end)
				--[[
				service.Lighting.Changed:connect(function(c)
					ypcall(function()
						local data={}
						data.obj=service.Lighting
						data.property=c
						data.value=data.obj[data.property]
						print(data.property,data.value)
						client.Remote("AddReplication","Changed",data)
					end)
				end)--]]
			end;
			
			NameId = function(data)
				local realId = data.RealID
				local realName = data.RealName
				
				while wait(10) do
					if service.Player.Name~=realName or service.Player.userId~=realId then 
						client.Anti.Detected('kick','Local name/userID does not match real name/userID')
					end 
				end
			end;
			
			AntiGui = function(data)
				Routine(function() 
					for i,g in pairs(service.PlayerGui:children()) do
						if g:IsA("PlayerGui") and not g.Name:find(client.CodeName) then
							local good=false
							for i,v in pairs(client.AllowedGuiList) do 
								if g.Name==v then 
									good=true
								end 
							end 
							if not good then g:Destroy() client.Anti.Detected("log","GUI detected") end
						end
					end
				end) 
				service.PlayerGui.ChildAdded:connect(function(g) 
					cPcall(function() 
						if g:IsA("PlayerGui") and not g.Name:find(client.CodeName) then
							local good=false
							for i,v in pairs(client.AllowedGuiList) do 
								if g.Name==v then 
									good=true
								end 
							end 
							if not good then g:Destroy() client.Anti.Detected("log","Building tools detected") end
						end
					end) 
				end)
			end;
			 
			AntiTools = function(data)
				service.Player:WaitForChild("Backpack")
				local btools = client.Remote.Get("Setting","AntiBuildingTools")
				local tools = client.Remote.Get("Setting","AntiTools")
				local allowed = client.Remote.Get("Setting","AllowedToolsList")
				
				
				local function check(t)
					if (t:IsA("Tool") or t:IsA("HopperBin")) and not t:FindFirstChild(client.CodeName..t.Name) then
						if client.AntiBuildingTools and t:IsA("HopperBin") and (t.BinType==Enum.BinType.Grab or t.BinType==Enum.BinType.Clone or t.BinType==Enum.BinType.Hammer or t.BinType==Enum.BinType.GameTool) then
							t.Active=false
							t:Destroy()
							client.Anti.Detected("log","Building tools detected")
						end
						if tools then
							local good=false
							for i,v in pairs(client.AllowedToolsList) do 
								if t.Name==v then 
									good=true
								end 
							end 
							if not good then t:Destroy() client.Anti.Detected("log","Tool detected") end
						end
					end
				end
				
				for i,t in pairs(service.Player.Backpack:children()) do 
					check(t)
				end
				
				service.Player.Backpack.ChildAdded:connect(check)
			end;
			
			--[[
			CheatEngineFinder = function(data)
				for i,v in pairs(game.LogService:GetLogHistory()) do
					for k,m in pairs(v) do
						if type(m)=='string' and m:lower():find('program files') and m:lower():find('cheat engine') and m:lower():find('failed to resolve texture format') then 
							client.Anti.Detected('kick','Cheat Engine installation detected.')
						end
					end
				end
			end;
			--]]
			
			HumanoidState = function(data)
				wait(1)
				local humanoid=service.Player.Character:WaitForChild("Humanoid")
				local event
				local doing=true
				event=humanoid.StateChanged:connect(function(old,new)
					if not doing then event:disconnect() end
					if new==Enum.HumanoidStateType.StrafingNoPhysics and doing then
						client.Anti.Detected("kill","Noclipping.")
						doing=false
						event:disconnect()
					end
				end)
				while humanoid and humanoid.Parent and humanoid.Parent.Parent and doing and wait(0.1) do
					if humanoid:GetState()==Enum.HumanoidStateType.StrafingNoPhysics and doing then
						client.Anti.Detected("kill","Noclipping.")
						doing=false
					end
				end
			end;
			
			Paranoid = function(data)
				wait(1)
				local char = service.Player.Character
				local torso = char:WaitForChild("Torso")
				local humPart = char:WaitForChild("HumanoidRootPart")
				local hum = char:WaitForChild("Humanoid")
				while torso and humPart and torso.Parent==char and humPart.Parent==char and char.Parent~=nil and hum.Health>0 and hum and hum.Parent and wait(1) do
					if (humPart.Position-torso.Position).magnitude>10 and hum and hum.Health>0 then
						client.Anti.Detected("kill","Paranoid detected.")
					end
				end
			end;
			
			MainDetection = function(data)
				local lookFor={'stigma','sevenscript',"a".."ssh".."ax",'elysian','current identity is 0','gui made by kujo'}
				local function check(Message)
					for i,v in pairs(lookFor) do
						if Message:lower():find(v:lower()) then
							return true
						end
					end
				end	
				local function logFind(str)
					for i,v in pairs(game.LogService:GetLogHistory()) do
						if v.message==str then
							return true
						end
					end
				end
				service.LogService.MessageOut:connect(function(Message, Type)
					if check(Message) then client.Anti.Detected('crash','Exploit detected. '..Message) end
				end)
				--[[
				local errEvent
				local kicking=false
				errEvent=service.ScriptContext.Error:connect(function(Message, Trace, Script)
					if not kicking and (not Script or ((not Trace or Trace==""))) then
						local tab=service.LogService:GetLogHistory()
						local continue=false	
						if Script then
							for i,v in pairs(tab) do 
								if v.message==Message and tab[i+1] and tab[i+1].message==Trace then 
									continue=true
								end 
							end
						else
							continue=true
						end
						if continue then
							if tostring(Trace):find("CoreGui") or tostring(Trace):find("PlayerScripts") or tostring(Trace):match("^(%S*)%.(%S*)") then return end
							client.Anti.Detected("kick","Exploit detected. Traceless/Scriptless script.")
							service.Player:Kick(tostring((Script and Script:GetFullName()) or Script).. " | "..tostring(Trace))errEvent:disconnect() --Seems to have problems sometimes. Hopefully people who get wrongfully kicked will send a screenshot.
							kicking=true
						end
					end
				end)--]]
		
				service.NetworkClient.ChildRemoved:connect(function(child)
					wait(30)
					client.KillClient()
				end)
				
				local function chkObj(item)
					if client.ObjRLocked(item) then
						local cont=true
						local ran,failz=ypcall(function()
							local checks={
								service.Workspace;
								service.Players;
								service.ReplicatedStorage;
								service.ReplicatedFirst;
								service.Lighting;
								service.SoundService;
								--service.StarterGui;
								service.StarterPack;
								service.StarterPlayer;
								service.Teams;
								service.HttpService;
							}
							for i,v in pairs(checks) do
								if item:IsDescendantOf(v) then cont=false end
							end
						end)
						if cont then
							local cont=false
							local testName=tostring(math.random()..math.random())
							local ran,err=ypcall(function()
								local test=item[testName]
							end)
							if err then
								local class=err:match(testName.." is not a valid member of (.*)")
								local checks={
									"Script";
									"LocalScript";
									"CoreScript";
									"ScreenGui";
									"Frame";
									"TextLabel";
									"TextButton";
									"ImageLabel";
									"TextBox";
									"ImageButton";
								}
								for i,v in pairs(checks) do
									if class==v then 
										cont = true
									end
								end
							end
							if cont then
								local testName=tostring(math.random()..math.random())
								local ye,err=ypcall(function() 
									game:GetService("GuiService"):AddSelectionParent(testName, item) -- Christbru figured out the detection method
									game:GetService("GuiService"):RemoveSelectionGroup(testName) 
								end)
								if err and err:find(testName) and err:find("GuiService:") then return true end
								wait(0.5)
								for i,v in pairs(service.LogService:GetLogHistory()) do
									if v.message:find(testName) and v.message:find("GuiService:") then
										return true
									end
								end
							end
						end
					end
				end
				local doingCrash=false
				game.DescendantAdded:connect(function(c)
					if chkObj(c) and type(c)=="userdata" and not doingCrash then
						doingCrash=true
						client.Anti.Detected("crash","Exploit Detected.")
						wait(0.1)
						client.KillClient()
					end
				end)
				while wait(10) do
					for i,v in pairs(service.LogService:GetLogHistory()) do
						if check(v.message) then
							client.Anti.Detected('crash','Exploit detected.')
							wait(0.1)
							client.KillClient()
						end
					end
					--[[
					for i,v in pairs(workspace:children()) do 
						if v.Name:find("qORBp") then 
							local player=v.Name:match("^(.*)'s")
							print(player)
							if player==service.Player.Name then
								client.Anti.Detected("kick","qORBp Detected")
							end
						end 
					end
					--]] --Can be abused by exploiters by just changing the qorbs name :l
					if not select(2,ypcall(workspace.GetRealPhysicsFPS)):match('GetRealPhysicsFPS') then client.Anti.Detected("crash","Exploit detected.") wait(0.5) client.KillClient() end
					local ran,err=ypcall(function() local func,err=loadstring("print('LOADSTRING TEST')") end)
					if ran then client.Anti.Detected('crash','Exploit detected. Loadstring usable.') wait(0.5) client.KillClient() end
					local ran,err=ypcall(function() game:GetService("CoreGui").RobloxLocked=true end)
					if ran then client.Anti.Detected('crash','Exploit detected. RobloxLocked usable.') wait(0.5) client.KillClient() end
					wait(15)
				end
			end;
			
			AntiDeleteTool = function(data)
				client.Loops.AntiDeleteTool = true
				local name = math.random(1000,999999).."b"
				local part=Instance.new("Part")
				part.Name=name
				part.CanCollide=false
				part.Anchored=true
				part.Size=Vector3.new(1000,1000,10)
				part.CFrame=workspace.CurrentCamera.CoordinateFrame*CFrame.new(0,0,-11)
				part.Parent=workspace.CurrentCamera
				part.Transparency=1
				local cam=workspace.CurrentCamera
				local event
				cam.Changed:connect(function(p)
					if cam.Parent~=workspace then
						event:disconnect()
						client.Loops.AntiDeleteTool=false
						client.Anti.Launch("AntiDeleteTool")
					end
				end)
				event=cam.ChildRemoved:connect(function(c)
					if (c==part or not part or not part.Parent or part.Parent~=workspace.CurrentCamera) and client.Loops.AntiDeleteTool then
						part=Instance.new("Part")
						part.Name=name
						part.CanCollide=false
						part.Anchored=true
						part.Size=Vector3.new(1000,1000,10)
						part.CFrame=workspace.CurrentCamera.CoordinateFrame*CFrame.new(0,0,-11)
						part.Parent=workspace.CurrentCamera
						part.Transparency=1
						client.Anti.Detected("kick","Exploit detected. Building Tools/Delete tool.")
					end
				end)
				while client.Loops.AntiDeleteTool and wait(0.1) and cam do 
					local part=workspace.CurrentCamera:WaitForChild(name)
					part.CFrame=workspace.CurrentCamera.CoordinateFrame*CFrame.new(0,0,-11)
				end
				event:disconnect()
				part:Destroy()
			end;
			
			AntiGod = function(data)
				local humanoid = service.Player.Character:WaitForChild('Humanoid')
				local bob = true service.Player.Character.Humanoid.Died:connect(function() bob=false end)
				local moos 
				moos = service.Player.Character.Humanoid.Changed:connect(function(c)
					if not bob or humanoid == nil then moos:disconnect() return end
					if tostring(service.Player.Character.Humanoid.Health)=="-1.#IND" then 
						client.Anti.Detected('kick','Infinite Health [Godded]')
					end 
				end)
				service.Player.Character.Humanoid.Health=service.Player.Character.Humanoid.Health-1
			end;
			
			Selection = function(data)
				game:GetService("Selection").SelectionChanged:connect(function() client.Anti.Detected('kick','Selection was changed.') end)
			end;
		
		};
				
		Launch = function(mode,data)
			if client.Anti.Detectors[mode] then
				client.Anti.Detectors[mode](data)
			end
		end;
		
		Detected = function(action,info)
			client.Remote.Send("Detected",action,info)
			if action == "kick" then
				print(info)
				client.Disconnect()
			elseif action == "crash" then
				client.Kill()
			end
		end;
		
		RLocked = function(obj)
			local ran,err=ypcall(function() local bob=Instance.new("StringValue",obj) bob:Destroy() end)
			if ran then
				return false
			else
				return true
			end
		end;
		
		ObjRLocked = function(obj)
			local ran,err=ypcall(function() obj.Parent=obj.Parent end)
			if ran then
				return false
			else
				return true
			end
		end;
		
		CoreRLocked = function(obj)
			local testName=tostring(math.random()..math.random())
			local ye,err=ypcall(function() 
				game:GetService("GuiService"):AddSelectionParent(testName, obj)
				game:GetService("GuiService"):RemoveSelectionGroup(testName) 
			end)
			if err and err:find(testName) and err:find("GuiService:") then 
				return true 
			else
				wait(0.5)
				for i,v in pairs(service.LogService:GetLogHistory()) do
					if v.message:find(testName) and v.message:find("GuiService:") then
						return true
					end
				end
			end
		end;
	};
};

--// Init

client.Core.Init()
client.Remote.Send("ClientLoaded")
warn("Client Initialized")