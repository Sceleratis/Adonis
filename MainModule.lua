-------------------
-- Adonis Server --
-------------------


--// Do away with the need for globals and empty the env

local _G = _G
local script = script
local game = game
local workspace = workspace
local getfenv = getfenv
local setfenv = setfenv
local getmetatable = getmetatable
local setmetatable = setmetatable
local math = math
local print = print
local warn = warn
local error = error
local coroutine = coroutine
local pcall = pcall
local ypcall = ypcall
local xpcall = xpcall
local select = select
local rawset = rawset
local rawget = rawget
local rawequal = rawequal
local pairs = pairs
local ipairs = ipairs
local next = next
local newproxy = newproxy
local os = os
local tick = tick
local loadstring = loadstring
local tostring = tostring
local tonumber = tonumber
local unpack = unpack
local string = string
local Instance = Instance
local type = type
local wait = wait
local require = require
local table = table
local Enum = Enum
local origEnv = getfenv()

setfenv(1,{})

--// Important variables

local server = {}
local settings = {}
local ErrorLogs = {} 
local function logError(plr,error) warn(error) table.insert(ErrorLogs,1,tostring(plr)..": "..tostring(error)) end
local print = function(...) for i,v in pairs({...}) do print(':: Adonis :: '..tostring(v)) end  end
local warn = function(...) for i,v in pairs({...}) do warn(':: Adonis :: '..tostring(v)) end end
local cPcall = function(func,...) local function cour(...) coroutine.resume(coroutine.create(func),...) end local ran,error=ypcall(cour,...) if error then logError("SERVER",error) end end
local Pcall = function(func,...) local ran,error=ypcall(func,...) if error then logError("SERVER",error) end end
local Routine = function(func,...) coroutine.resume(coroutine.create(func),...) end
local service; service = setmetatable({
		MarketPlace = game:service("MarketplaceService");
		GamepassService = game:service("GamePassService");
		ChatService = game:service("Chat");
		GetPlayers = function(plr, names, donterror, isServer)
			local players = {} 
			local prefix = settings.SpecialPrefix
			if isServer then prefix = "" end
			local parent = service.NetworkServer or service.Players
			for s in names:gmatch('([^,]+)') do
				local plrs = 0
				local function plus()
					plrs = plrs+1
				end
				local function getplr(p)
					if p:IsA('NetworkReplicator') then
						if p:GetPlayer()~=nil and p:GetPlayer():IsA('Player') then
							p=p:GetPlayer()
						end
					end
					return p
				end
				local function randomPlayer()
					if #players>=#parent:children() then return end
					local rand = parent:children()[math.random(#parent:children())]
					local p=getplr(rand)
					for i,v in pairs(players) do
						if(v==p.Name)then
							randomPlayer()
							return;
						end
					end
					table.insert(players,p)
					plus();
				end
				if s:lower()==prefix..'me' and plr then
					table.insert(players,plr)
					plus()
				elseif s:lower()==prefix..'all' then
					for i,v in pairs(parent:children()) do
						local p=getplr(v)
						table.insert(players,p)
						plus()
					end
				elseif s:lower()==prefix..'others' then
					for i,v in pairs(parent:children()) do
						local p=getplr(v)
						if p~=plr then
							table.insert(players,p)
							plus()
						end
					end
				elseif s:lower()==prefix..'random' then
					randomPlayer();
					plus()
				elseif s:lower()==prefix..'admins' then
					for i,v in pairs(parent:children()) do
						local p=getplr(v)
						if server.CheckAdmin(p,false) then
							table.insert(players,p)
							plus()
						end
					end
				elseif s:lower()==prefix..'nonadmins' then
					for i,v in pairs(parent:children()) do
						local p=getplr(v)
						if not server.CheckAdmin(p,false) then
							table.insert(players,p)
							plus()
						end
					end
				elseif s:lower()==prefix..'friends' then
					for i,v in pairs(parent:children()) do
						local p=getplr(v)
						if p:IsFriendsWith(plr.userId) then
							table.insert(players,p)
							plus()
						end
					end
				elseif s:lower()==prefix..'besties' then
					for i,v in pairs(parent:children()) do
						local p=getplr(v)
						if p:IsBestFriendsWith(plr.userId) then
							table.insert(players,p)
							plus()
						end
					end
				elseif s:lower():sub(1,1)=='%' then
					for i,v in pairs(service.Teams:children()) do
						if v.Name:lower():sub(1,#s:sub(2))==s:lower():sub(2) then
							for k,m in pairs(parent:children()) do
								local p=getplr(m)
								if p.TeamColor==v.TeamColor then
									table.insert(players,p)
									plus()
								end
							end
						end
					end
				elseif s:lower():sub(1,1)=='$' then
					for i,v in pairs(parent:children()) do
						local p=getplr(v)
						if tonumber(s:lower():sub(2)) then
							if p:IsInGroup(tonumber(s:lower():sub(2))) then
								table.insert(players,p)
								plus()
							end
						end
					end
				elseif s:lower():sub(1,3)=='id-' then
					for i,v in pairs(parent:children()) do
						local p = getplr(v)
						if tonumber(s:lower():sub(4)) then
							if p and p.userId==tonumber(s:lower():sub(4)) then
								table.insert(players,p)
								plus()
							end
						end
					end
				elseif s:lower():sub(1,5)=='team-' then
					for i,v in pairs(service.Teams:children()) do
						if v.Name:lower():sub(1,#s:sub(6))==s:lower():sub(6) then
							for k,m in pairs(parent:children()) do
								local p=getplr(m)
								if p.TeamColor==v.TeamColor then
									table.insert(players,p)
									plus()
								end
							end
						end
					end
				elseif s:lower():sub(1,6)=='group-' then
					for i,v in pairs(parent:children()) do
						local p=getplr(v)
						if tonumber(s:lower():sub(7)) then
							if p:IsInGroup(tonumber(s:lower():sub(7))) then
								table.insert(players,p)
								plus()
							end
						end
					end
				elseif s:lower():sub(1,1)=='-' then
					local removes=server.GetPlayers(plr,s:lower():sub(2),true)
					for i,v in pairs(players) do
						for k,p in pairs(removes) do
							if v.Name==p.Name then
								table.remove(players,i)
								plus()
							end
						end
					end
				elseif s:lower():sub(1,1)=='#' then
					local num = tonumber(s:lower():sub(2))
					if(num==nil)then
						server.OutputGui(plr,'','Invalid number!')
					end
					for i=0,num do
						randomPlayer();
					end
				elseif s:lower():sub(1,7)=="radius-" then
					local num = tonumber(s:lower():sub(8))
					if(num==nil)then
						server.OutputGui(plr,'','Invalid number!')
					end
					for i,v in pairs(parent:children()) do
						local p=getplr(v)
						if p~=plr and plr:DistanceFromCharacter(p.Character.Head.Position)<=num then
							table.insert(players,p)
							plus()
						end
					end
				else
					for i,v in pairs(parent:children()) do
						local p=getplr(v)
						if p.Name:lower():sub(1,#s)==s:lower() then
							table.insert(players,p)
							plus()
						end
					end
				end
				if plrs==0 and not donterror then server.OutputGui(plr,'','No players matching '..s..' were found!') end
			end
			return players
		end
	},{
	__index = function(tab,index)
		local serv
		local ran,err = ypcall(function() serv = game:GetService(index) end)
		if ran then
			return serv
		end
	end
})



--// Setting things up
server = {
	Service = service;
	Print = print;
	Warn = warn;
	cPcall = cPcall;
	Pcall = Pcall;
	Routine = Routine;
	LogError = logError;
	ErrorLogs = ErrorLogs;
	Deps = script.Dependencies.Server;
	ClientDeps = script.Dependencies.Client;
	
	Logs = {
		Admin = {};
		Chats = {};
		Joins = {};
		Replications = {};
		Commands = {};
		Exploit = {};
	};
	
	Variables = {
		DonorPass = {159549976,157092510};
		LightingSettings = {
			Ambient = service.Lighting.Ambient;
			OutdoorAmbient = service.Lighting.OutdoorAmbient;
			Brightness = service.Lighting.Brightness;
			TimeOfDay = service.Lighting.TimeOfDay;
			FogColor = service.Lighting.FogColor;
			FogEnd = service.Lighting.FogEnd;
			FogStart = service.Lighting.FogStart;
			GlobalShadows = service.Lighting.GlobalShadows;
			Outlines = service.Lighting.Outlines;
			ShadowColor = service.Lighting.ShadowColor;
			ColorShift_Bottom = service.Lighting.ColorShift_Bottom;
			ColorShift_Top = service.Lighting.ColorShift_Top;
			GeographicLatitude = service.Lighting.GeographicLatitude;
			Name = service.Lighting.Name
		};
		Whitelist = {
			Enabled = true;
			List = {
				
			};
		};
	};
	
	--// Remote
	Remote = {
		LoadingPlayers = {};
		Returns = {};
		
		Returnables = {
			Test = function(p,args)
				return "HELLO FROM THE OTHER SIDE :)!"
			end;
			
			Ping = function(p,args)
				return "Pong"
			end;
			
			Variable = function(p,args)
				return server.Variables[args[1]]
			end;
			
			Setting = function(p,args)
				local setting = args[1]
				local blocked = {
					DataStore = true;
					
					Trello_Enabled = true;	
					Trello_PrimaryBoard = true;
					Trello_SecondaryBoards = true;
					Trello_AppKey = true;
					Trello_Token = true;
					
					G_Access = true;
					G_Access_Key = true;
					G_Access_Perms = true;
					Allowed_API_Calls = true;
				}
				
				if blocked[setting] and not server.Admin.GetLevel(p)>=5 then 
					return nil
				elseif settings[setting] then
					return settings[setting]
				end
			end;
			
			AllSettings = function(p,args)
				if server.Admin.GetLevel(p)>=4 then
					local sets = {}
					local descs = {}					
					descs.DataStore = [[DataStore the script will use for saving data; Changing this will lose any saved data ]]
	
					descs.Theme = [[UI theme ]]
					descs.MobileTheme = [[Theme to use on mobile devices ]]
					
					descs.Helpers = [[Helpers; Previously "Mods"				  Format: {"Username"; "Username:UserId"; UserId; "Group:GroupId:GroupRank"; "Item:ItemID";} ]]
					descs.Moderators = [[Mods; Previously "Admins"                  Format: {"Username"; "Username:UserId"; UserId; "Group:GroupId:GroupRank"; "Item:ItemID";} ]]
					descs.Admins = [[Administrators; Previously "Owners"		  Format: {"Username"; "Username:UserId"; UserId; "Group:GroupId:GroupRank"; "Item:ItemID";} ]]
					descs.Owners = [[Anyone to be identified as a place owner;  Format: {"Username"; "Username:UserId"; UserId; "Group:GroupId:GroupRank"; "Item:ItemID";} ]]
					descs.Banned = [[List of people banned from the game 		  Format: {"Username"; "Username:UserId"; UserId; "Group:GroupId:GroupRank"; "Item:ItemID";} ]]
					descs.BlackList = [[List of people banned from using admin 	  Format: {"Username"; "Username:UserId"; UserId; "Group:GroupId:GroupRank"; "Item:ItemID";} ]]
					
					descs.Prefix = [[The : in :kill me ]]
					descs.PlayerPrefix = [[The ! in !donate; Mainly used for commands that any player can run ]]
					descs.ConsoleKey = [[Keybind used to open the console ]]					
					descs.SplitKey = [[The space in :kill me (eg if you change it to / :kill me would be :kill/me) ]]
					descs.BatchKey = [[:kill me | :ff bob | :explode scel ]]
					descs.SpecialPrefix = [[Used for things like "all", "me" and "others" (If changed to ! you would do :kill !me) ]]
					
					descs.HttpWait = [[How long things that use the HttpService will wait before updating again ]]
					descs.Trello_Enabled = [[Are the Trello features enabled? ]]
					descs.Trello_Primary = [[Primary Trello board ]]
					descs.Trello_Secondary = [[Secondary Trello boards			          Format: {"BoardID";} ]]
					descs.Trello_AppKey = [[Your Trello AppKey ]]
					descs.Trello_Token = [[Trello token (DON'T SHARE WITH ANYONE!) ]]
					
					descs.CreatorPowers = [[When true gives me place owner admin; This is strictly used for debugging; I can't debug without access to the script and specific owner commands ]]
					descs.FunCommands = [[Are fun commands enabled? ]]
					descs.ChatCommands = [[If false you will not be able to run commands via the chat; Instead you MUST use the console or you will be unable to run commands ]]
					descs.SaveAdmins = [[If true anyone you :mod, :admin, or :owner in-game will save; This does not apply to helpers as they are temporary ]]
					
					descs.CustomChat = [[Custom chat]]
					descs.PlayerList = [[Custom playerlist ]]
					
					descs.DonorCommands = [[Show your support for the script and let donors use commands like !sparkles]]
					descs.DonorCapes = [[Donors will always have capes; This just determines if other people can see them]]
					
					descs.Detection = [[Attempts to detect certain known exploits]]
					
					sets.Settings = settings
					sets.Descs = descs
					
					return sets
				end
			end;
			
			AdminLevel = function(p,args)
				return server.Admin.GetLevel(p)
			end;
			
			Keybinds = function(p,args)
				local playerData = server.Core.GetData(tostring(p.userId))
				return playerData.Keybinds or {}
			end;
			
			UpdateKeybinds = function(p,args)
				local playerData = server.Core.GetData(tostring(p.userId))
				local binds = args[1]
				local resp = "OK"
				if type(binds)=="table" then
					playerData.Keybinds = binds
					server.Core.SaveData(tostring(p.userId),playerData)
					resp = "Updated"
				else
					resp = "Error"
				end
				return resp
			end;
			
			UpdateDonor = function(p,args)
				local playerData = server.Core.DataStore:GetAsync(tostring(p.userId))
				local donor = args[1]
				local resp = "OK"
				if type(donor)=="table" and donor.Cape and type(donor.Cape)=="table" then
					playerData.Donor = donor
					server.Core.DataStore:SetAsync(tostring(p.userId),playerData)
					if donor.Enabled then
						server.Admin.Donor(p)
					else
						server.Functions.UnCape(p)
					end
					resp = "Updated"
				else
					resp = "Error"
				end
				return resp
			end;
			
			PlayerData = function(p,args)
				local data = server.Core.DataStore:GetAsync(tostring(p.userId)) 
				data.isDonor = server.Admin.CheckDonor(p)
				return data
			end;
			
			CheckAdmin = function(p,args)
				return server.Admin.CheckAdmin(p)
			end;
			
			SearchCommands = function(p,args)
				return server.Admin.SearchCommands(p,args[1] or "all")
			end;
			
			FormattedCommands = function(p,args)
				local commands = server.Admin.SearchCommands(p,args[1] or "all")
				local tab = {}
				for i,v in pairs(commands) do
					table.insert(tab,server.Admin.FormatCommand(v))
				end
				return tab
			end;
			
			Terminal = function(p,args) 
				local output = {}
				
				local function out(put)
					table.insert(output,put)
				end
				
				local commands 
				commands = {
					help = function(norg)
						out("Commands: ")
						for i,v in pairs(commands) do
							out(i)
						end
					end;
					test = function(norg)
						out("GOT A RETURN! "..os.time())
					end;
					code = function(norg)
						local newenv = server.Core.GetEnv(origEnv)
						newenv.print = function(...) local nums={...} for i,v in pairs(nums) do out(tostring(v)) end end
						local func,err=server.Core.Loadstring(norg,newenv)
						if func then 
							func()
						else
							out(err:match(":(.*)") or err)
						end
					end;
					run = function(norg)
						server.Process.Command(p,norg,true,true)
						out("Command Ran: "..norg)
					end;
					kick = function(norg)
						local plrs = server.GetPlayers(p,norg)
						if #plrs>0 then
							for i,v in pairs(plrs) do
								v:Kick("You have been kicked by the server")
								out("Kicked "..v.Name)
							end
						else
							out("No players matching '"..norg.."' found!")
						end
					end;
					kill = function(norg)
						local plrs = server.GetPlayers(p,norg)
						if #plrs>0 then
							for i,v in pairs(plrs) do
								v.Character:BreakJoints()
								out("Killed "..v.Name)
							end
						else
							out("No players matching '"..norg.."' found!")
						end
					end;
					respawn = function(norg)
						local plrs = server.GetPlayers(p,norg)
						if #plrs>0 then
							for i,v in pairs(plrs) do
								v:LoadCharacter()
								out("Respawned "..v.Name)
							end
						else
							out("No players matching '"..norg.."' found!")
						end
					end;
					shutdown = function(norg)
						for i,v in pairs(service.Players:GetPlayers()) do
							v:Kick()
						end
						service.PlayerAdded:connect(function(p)
							p:Kick()
						end)
					end;
					serverlogs = function(norg)
						server.Process.Command(p,":serverlogs",true,true)
						out("Openning server logs...")
					end;
					clientlogs = function(norg)
						if not norg then norg = server.SpecialPrefix.."me" end
						server.Process.Command(p,":clientlogs"..server.SplitKey..norg,true,true)
						out("Openning client logs...")
					end;
				}
				
				if server.Admin.GetLevel(p)>=4 then
					
					local command = args[1]:match("(.-) ") or args[1]
					local norgs = args[1]:match(" (.*)")
					
					if commands[command] then
						local ran,err = ypcall(commands[command],norgs)
						if err then
							out(err:match(":(.*)") or err)
						end
					else 
						out(command.." is not a valid command")
					end
				end
				return output
			end;
		};
		
		UnEncrypted = {
			AddReplication = function(p,args)
				local na = "_SERVER"
				local action = args[1]
				local data = args[2]
				local datat
				
				print(p,action)
				
				if p then na = p.Name end
				
				if action == "Created" then
					local obj=data.obj
					local name = data.name
					local class = data.class
					local parent=data.parent
					local path=data.path
					if not obj then 
						datat = {Action=action,Parent=parent,ClassName=class,Player=na,Object=obj,Name=name,Path=path}
					else
						datat = {Action=action,Parent=parent,ClassName=class,Player="_SERVER",Object=obj,Name=name,Path=path}
					end
				elseif action == "Destroyed" then
					local obj=data.obj
					local name = data.name
					local class = data.class
					local parent = data.parent
					local path = data.path
					if not obj or not obj.Parent then 
						datat = {Action=action,Parent=parent,ClassName=class,Player="_SERVER",Object=obj,Name=name,Path=path}
					elseif obj and obj.Parent then
						datat = {Action=action,Parent=parent,ClassName=class,Player=na,Object=obj,Name=name,Path=path}
					else
						datat = {Action=action,Parent=parent,ClassName=class,Player="_UNKNOWN",Object=obj,Name=name,Path=path}
					end
				elseif action == "Changed" then
					local obj = data.obj
					local prop = data.property
					local newValue = data.value
					if obj[prop] ~= newValue then
						print("PLAYER")
						datat = {Action=action,Parent=obj.Name,ClassName=obj.ClassName,Player=na,Object=obj,Name="PROPERTY: "..prop,Path=obj:GetFullName()}
					else
						print("SERVER")
						datat = {Action=action,Parent=obj.Name,ClassName=obj.ClassName,Player="_SERVER",Object=obj,Name="PROPERTY: "..prop,Path=obj:GetFullName()}
					end
				elseif action=="LChanged" then
					datat = {Action=action,Parent="Lighting",ClassName="Lighting",Player=na,Object=service.Lighting,Name="PROPERTY: "..data.Property,Path="service.Lighting"}
				end
				if datat and datat.Player~="_SERVER" then
					table.insert(server.FirstReplicators,1,datat)
					if #server.FirstReplicators>1000 then
						table.remove(server.FirstReplicators,#server.FirstReplicators)
					end
				end
			end;
		};
		
		Commands = {
			GetReturn = function(p,args)
				local com = args[1]
				local key = args[2]
				local parms = {args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10],args[11],args[12]} -- unpack() issues..
				local retable = server.Remote.Returnables[com]
				local ret
				if retable then
					ret = retable(p,parms)
				else
					ret = nil
				end
				server.Remote.Send(p,"GiveReturn",key,ret)
			end;
			
			GiveReturn = function(p,args)
				local key = args[1]
				server.Remote.Returns[key] = {args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10],args[11]}
			end;
			
			SaveTableAdd = function(p,args) 
				if server.Admin.GetLevel(p)>=4 then
					local tab = args[1]
					local value = args[2]
					
					table.insert(settings[tab],value)
					
					server.Core.DoSave({
						Type = "TableAdd";
						Table = tab;
						Value = value;
					})
					 
				end
			end;
			
			SaveTableRemove = function(p,args) 
				if server.Admin.GetLevel(p)>=4 then
					local tab = args[1]
					local value = args[2]
					local ind = server.Functions.GetIndex(settings[tab],value)
					
					if ind then table.remove(settings[tab],ind) end
					
					server.Core.DoSave({
						Type = "TableRemove";
						Table = tab;
						Value = value;
					})
				end
			end;
			
			SaveSetSetting = function(p,args) 
				if server.Admin.GetLevel(p)>=4 then
					local setting = args[1]
					local value = args[2]
					
					settings[setting] = value
					
					if setting=='Prefix' or setting=='AnyPrefix' or setting=='SpecialPrefix' then
						local orig=settings[setting]
						for i,v in pairs(server.Admin.Commands) do
							if v.Prefix==orig then
								v.Prefix = settings[setting]
							end
						end
					end
				
					server.Core.DoSave({
						Type = "SetSetting";
						Setting = setting;
						Value = value;
					})
				end
			end;
			
			ClearSavedSettings = function(p,args) 
				if server.Admin.GetLevel(p)>=4 then
					server.Core.DoSave({Type = "ClearSettings"})
				end
			end;
			
			SetSetting = function(p,args) 
				if server.Admin.GetLevel(p)>=4 then
					settings[args[1]]=args[2]
					if args[1]=='Prefix' or args[1]=='AnyPrefix' or args[1]=='SpecialPrefix' then
						local orig = server[args[1]]
						for i,v in pairs(server.Commands) do
							if v.Prefix == orig then
								v.Prefix = settings[args[1]]
							end
						end
					end
				end
			end;
			
			Detected = function(p,args)
				server.Anti.Detected(p,args[1],args[2])
			end;
			
			TrelloOperation = function(p,args)
				if not server.Admin.GetLevel(p)>2 then return end
				local op = args[1]
				if op=="MakeCard" then
					local list = args[2]
					local name = args[3]
					local desc = args[4]
					local trello = server.HTTP.Trello.API(server.TrelloAppKey,server.TrelloToken)
					local lists = trello.getLists(server.BoardID)
					local list = trello.getListObj(lists,list)
					if list then
						local card = trello.makeCard(list.id,name,args[4])
						server.Hint("Made card \""..card.name.."\"",{p})
					end
				end
			end;
			
			ClientLoaded = function(p,args)
				server.Remote.LoadingPlayers[tostring(p.userId)] = nil
			end;
			
			LogError = function(p,args)
				logError(p,args[1])
			end;
			----
			
			Test = function(p,args)
				print("OK WE GOT COMMUNICATION! FROM: "..p.Name.." ORGL: "..args[1])
			end;
			
			ProcessCommand = function(p,args)
				server.Process.Command(p,args[1],{Check=true})	
			end;
			
			ProcessChat = function(p,args)
				server.Process.Chat(p,args[1])
				server.Process.CustomChat(p,args[1])
				if settings.ChatCommands then
					server.Process.Command(p,args[1])
				end
			end;
			
			ProcessCustomChat = function(p,args)
				server.Process.Chat(p,args[1])
				server.Process.CustomChat(p,args[1],args[2])
				if settings.ChatCommands then
					server.Process.Command(p,args[1])
				end
			end;
			
			PrivateMessage = function(p,args)
			--	'Reply from '..localplayer.Name,player,localplayer,ReplyBox.Text
				local title = args[1]
				local target = args[2]
				local from = args[3]
				local message = args[4]
				server.Remote.MakeGui(target,"PM",{
					Title = title;
					Player = p;
					Message = message;
				})
			end;
			
		};
		
		
		Fire = function(...)
			if server.Core.RemoteEvent and server.Core.RemoteEvent.Object then
				server.Core.RemoteEvent.Object:FireClient(...)
			end
		end;
		
		Send = function(p,com,...)
			server.Remote.Fire(p,server.Remote.Encrypt(com,server.Core.Key),...)
		end;
		
		Get = function(p,com,...)
			local key = server.Functions:GetRandom()
			server.Remote.Returns[key] = key
			local t = os.time()
			server.Remote.Send(p,"GetReturn",com,key,...)
			repeat wait(0.1) until server.Remote.Returns[key] ~= key or os.time()-t>300
			local args = server.Remote.Returns[key]
			if args == key then
				return nil
			elseif type(args)=="table" then
				return args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10]	
			end
		end;
		
		MakeGui = function(p,GUI,data,themeData)
			local theme = themeData or {Desktop = settings.Theme; Mobile = settings.MobileTheme}
			server.Remote.Send(p,"UI",GUI,theme,data  or {})
		end;
		
		MakeGuiGet = function(p,GUI,data,themeData)
			local theme = themeData or {Desktop = settings.Theme; Mobile = settings.MobileTheme}
			return server.Remote.Get(p,"UI",GUI,theme,data or {})
		end;
		
		Encrypt = function(str,key)
			local keyBytes = {}
			local strBytes = {}
			local endStr = ""
			for i=1,#key do table.insert(keyBytes,string.byte(key:sub(i,i))) end 
			for i=1,#str do table.insert(strBytes,string.byte(str:sub(i,i))) end 
			for i=1,#strBytes do
				if i%#keyBytes>0 then
					if strBytes[i]+keyBytes[i%#keyBytes]>255 then
						strBytes[i]=math.abs(strBytes[i]-keyBytes[i%#keyBytes])
					else
						strBytes[i]=math.abs(strBytes[i]+keyBytes[i%#keyBytes])
					end
				else
					if strBytes[i]+keyBytes[1]>255 then
						strBytes[i]=math.abs(strBytes[i]-keyBytes[1])
					else
						strBytes[i]=math.abs(strBytes[i]+keyBytes[1])
					end
				end
			end
			for i=1,#strBytes do endStr=endStr..string.char(strBytes[i]) end
			return endStr
		end;
		
		Decrypt = function(str,key)
			local keyBytes = {}
			local strBytes = {}
			local endStr = ""
			for i=1,#key do table.insert(keyBytes,string.byte(key:sub(i,i))) end 
			for i=1,#str do table.insert(strBytes,string.byte(str:sub(i,i))) end 
			for i=1,#strBytes do
				if i%#keyBytes>0 then
					if strBytes[i]+keyBytes[i%#keyBytes]>255 then
						strBytes[i]=math.abs(strBytes[i]-keyBytes[i%#keyBytes])
					else
						strBytes[i]=math.abs(keyBytes[i%#keyBytes]-strBytes[i])
					end
				else
					if strBytes[i]+keyBytes[1]>255 then
						strBytes[i]=math.abs(strBytes[i]-keyBytes[1])
					else
						strBytes[i]=math.abs(keyBytes[1]-strBytes[i])
					end
				end
			end
			for i=1,#strBytes do endStr=endStr..string.char(strBytes[i]) end
			return endStr
		end;
	
	};
	
	
	--// Core
	Core = {
		MakeEvent = function()
			local ran,error = ypcall(function()
				if server.Anti.RLocked(service.ReplicatedStorage) then
					server.Core.PanicMode("ReplicatedStorage RobloxLocked")
				else
					if server.Core.RemoteEvent then
						server.Core.RemoteEvent.Security:disconnect()
						server.Core.RemoteEvent.Event:disconnect()
						pcall(function() server.Core.RemoteEvent.Object:Destroy() end)
					end
					server.Core.RemoteEvent = {}
					local event = Instance.new("RemoteEvent")
					event.Name = server.Core.Name
					event.Archivable = false
					event.Parent = service.ReplicatedStorage
					server.Core.RemoteEvent.Object = event
					server.Core.RemoteEvent.Event = event.OnServerEvent:connect(server.Process.Remote)
					server.Core.RemoteEvent.Security = event.Changed:connect(function(p)
						if p=="RobloxLocked" and server.Anti.ObjRLocked(event) then
							server.Core.PanicMode("RemoteEvent RobloxLocked")
						elseif p=="Name" then
							event.Name = server.Core.Name
						elseif p=="Parent" then
							server.Core.MakeEvent()
						end
					end)
				end
			end)
			if error then
				print(error)
				server.Core.PanicMode("Error while making RemoteEvent")
			end
		end;
		
		CheckClient = function(p)
			local ret = server.Remote.Get(p,"ClientHooked")
			if ret == server.Core.Name then
				return true 
			else
				return false
			end
		end;
		
		CheckAllClients = function()
			for ind,p in pairs(service.Players:GetPlayers()) do
				Routine(function()
					if server.Core.CheckClient(p) then
						-- yay
					else
						server.Anti.Detected(p,"kick","Client did not reply in time")
					end
				end)
			end
		end;
		
		HookClient = function(p)
			local client = server.Deps.Client:clone()
			client.Name = server.Core.Name
			client.Parent = script			
			
			local deps = server.ClientDeps:Clone()
			deps.Name = "Deps"
			deps.Parent = client
			
			local ok,err = pcall(function() 
				local t = os.time()
				repeat client.Parent = p:FindFirstChild("Backpack") wait(1/60) until os.time()-t>60 or (client.Parent and type(client.Parent) == "userdata" and tostring(client.Parent) == "Backpack" and client.Parent:IsA("Backpack"))
				if os.time()-t>60 then
					error("Couldn't load")
				else
					client.Disabled = false
				end
			end)
			
			if not ok or not client then 
				p:Kick("Error loading client") 
				return false
			else
				local t = os.time() 
				repeat 
					wait(1/60)
				until not server.Remote.LoadingPlayers[tostring(p.userId)] or os.time()-t>300
				print(tick())
				if server.Remote.LoadingPlayers[tostring(p.userId)] then
					p:Kick("Client did not load in time")
					return false
				else
					return true
				end
			end
			return false
		end;
		
		MakeClient = function()
			local ran,error = ypcall(function()
				if server.Anti.RLocked(service.StarterPlayer) then
					server.Core.PanicMode("StarterPlayer RobloxLocked")
				else
					local starterScripts = service.StarterPlayer:FindFirstChild(server.Core.Name)
					if not starterScripts then
						starterScripts = Instance.new("StarterPlayerScripts",service.StarterPlayer)
						starterScripts.Name = server.Core.Name
						starterScripts.Changed:connect(function(p)
							if p=="Parent" then
								server.MakeClient()
							elseif p=="Name" then
								starterScripts.Name = server.Core.Name
							elseif p=="RobloxLocked" and server.Anti.RLocked(starterScripts) then
								server.Core.PanicMode("PlayerScripts RobloxLocked")
							end
						end)
						starterScripts.ChildAdded:connect(function(c)
							if c.Name~=server.Core.Name then
								wait(0.5)
								c:Destroy()
							end
						end)
					end
					starterScripts:ClearAllChildren()
					if server.Anti.RLocked(starterScripts) then
						server.Core.PanicMode("StarterPlayerScripts RobloxLocked")
					else
						if server.Core.Client then
							local cli = server.Core.Client
							if server.Anti.ObjRLocked(cli.Object) then
								server.Core.PanicMode("Client RobloxLocked")
							else
								server.Core.Client.Security:disconnect()
								pcall(function() server.Core.Client.Object:Destroy() end)
							end
						end
						server.Core.Client = {}
						local client = server.Deps.Client:clone()
						client.Name = server.Core.Name
						server.ClientDeps:Clone().Parent = client
						client.Parent = starterScripts
						client.Disabled = false
						server.Core.Client.Object = client
						server.Core.Client.Security = client.Changed:connect(function(p)
							if p == "Parent" or p == "RobloxLocked" then
								server.Core.MakeClient()
							end
						end)
					end
				end
			end)
			if error then
				print(error)
				server.Core.PanicMode("Error while making client")
			end
		end;
		
		GetEnv = function(env)
			local scriptEnv = setmetatable({},{
				__index = function(tab,ind)
					local e = env
					if e[ind] then
						return e[ind]
					end
				end
			})
			
			scriptEnv.print = print
			scriptEnv.warn = warn
			
			scriptEnv.server = server
			scriptEnv.service = service
			scriptEnv.settings = settings
			
			scriptEnv.cPcall = cPcall
			scriptEnv.Pcall = Pcall
			scriptEnv.Routine = Routine
			
			scriptEnv.ErrorLogs = ErrorLogs
			scriptEnv.logError = logError
			
			return scriptEnv
		end;
		
		LoadPlugin = function(plugin)
			local plug = require(plugin)
			local func = setfenv(plug,server.Core.GetEnv(getfenv(plug)))
			cPcall(func)
		end;
		
		DoSave = function(data)
			local type = data.Type
			if type == "ClearSettings" then
				server.Core.SetData("SavedSettings",{})
				server.Core.SetData("SavedTables",{})
			elseif type == "SetSetting" then
				local setting = data.Setting
				local value = data.Value
				local sets = server.Core.GetData("SavedSettings")
				local special
				
				if setting == "TopbarColor" then
					local newColor = value
					special = {newColor.r,newColor.g,newColor.b}
				else
					special = value
				end

				sets[setting] = special
				server.Core.SaveData("SavedSettings",sets) 
				
				return true
			elseif type == "TableRemove" then
				local tab = data.Table
				local value = data.Value
				local sets = server.Core.GetData("SavedTables")
				local tabInd
				
				for i,v in pairs(sets) do 
					if tab==v.Table then
						if server.Functions.CheckMatch(v.Value,value) then
							tabInd = i
						end
					end
				end
				if tabInd then 
					table.remove(sets,tabInd)
				end
				table.insert(sets,{Table=tab,Action="Remove",Value=value})
				server.Core.SaveData("SavedTables",sets)
			elseif type == "TableAdd" then
				local tab = data.Table
				local value = data.Value
				
				local sets = server.Core.DataStore:GetAsync("SavedTables")
				local tabInd
				for i,v in pairs(sets) do 
					if value==v.Table then
						if server.Functions.CheckMatch(v.Value,value) then
							tabInd = i
						end
					end
				end
				if tabInd then 
					table.remove(sets,tabInd)
				end
				table.insert(sets,{Table=tab,Action="Add",Value=value})
				server.Core.DataStore:SetAsync("SavedTables",sets)
			end
		end;
		
		SaveData = function(key,value)
			server.Core.DataStore:SetAsync(key,value)
		end;
		
		GetData = function(key)
			return server.Core.DataStore:GetAsync(key)
		end;
		
		LoadData = function()
			local SavedSettings = server.Core.GetData("SavedSettings")
			local SavedTables = server.Core.GetData("SavedTables")
			
			if not SavedSettings then 
				SavedSettings = {} 
				server.Core.SaveData("SavedSettings",{}) 
			end
			
			if not SavedTables then 
				SavedTables = {} 
				server.Core.SaveData("SavedTables",{}) 
			end
			
			for setting,value in pairs(SavedSettings) do
				local good = true
				if type(value)=="table" and setting ~= "TopbarColor" then
					good = false
				end
				if good then
					local special = value
					if setting == "TopbarColor" then
						special = Color3.new(value[1],value[2],value[3])
					end
					
					settings[setting] = special
				end
			end
			
			for ind,tab in pairs(SavedTables) do
				if settings[tab.Table] then
					if tab.Action=="Add" then
						table.insert(settings[tab.Table],tab.Value)
					elseif tab.Action=="Remove" then
						local ind = server.Functions.GetIndex(settings[tab.Table],tab.Value)
						if ind then
							table.remove(settings[tab.Table],ind)
						end
					end
				end
			end
		end;
		
		StartAPI = function()
			local setmetatable = setmetatable
			local type = type
			local error = error
			local API_Special = {
				AddAdmin = settings.Allowed_API_Calls.DataStore;
				RemoveAdmin = settings.Allowed_API_Calls.DataStore;
				RunCommand = settings.Allowed_API_Calls.Core;
				SaveTableAdd = settings.Allowed_API_Calls.DataStore and settings.Allowed_API_Calls.Settings;
				SaveTableRemove = settings.Allowed_API_Calls.DataStore and settings.Allowed_API_Calls.Settings;
				SaveSetSetting = settings.Allowed_API_Calls.DataStore and settings.Allowed_API_Calls.Settings;
				ClearSavedSettings = settings.Allowed_API_Calls.DataStore and settings.Allowed_API_Calls.Settings;
				SetSetting = settings.Allowed_API_Calls.Settings;
			}
			
			local API_Specific = {
				API_Specific = {
					Test = function()
						print("We ran the api specific stuff")
					end
				};
				Settings = settings;
			}
			
			local API = {
				Access = function(...)
					local args = {...}
					local key = args[1]
					local ind = args[2]
					local targ
					
					if API_Specific[ind] then 
						targ = API_Specific[ind] 
					elseif server[ind] and settings.Allowed_API_Calls[ind] then
						targ = server[ind]
					end
					
					if key == settings.G_Access_Key and targ and settings.Allowed_API_Calls[ind] then
						if type(targ) == "table" then
							return setmetatable({},{
								__index = function(tab,inde)
									if targ[inde] ~= nil and API_Special[inde] == nil or API_Special[inde] == true then
										return targ[inde]
									elseif API_Special[inde] == false then
										error("Access Denied: "..tostring(inde))
									else
										error("Could not find "..tostring(inde))
									end
								end;
								__newindex = function(tabl,inde,valu)
									if settings.G_Access_Perms == "Read" then
										error("Read-only")
									elseif settings.G_Access_Perms == "Write" then
										tabl[inde] = valu
									end
								end;
								__metatable = false;
							})
						end
					else
						error("Incorrect access key")
					end
				end
			}
			
			setfenv(API.Access,{})
			
			AdonisGTable = setmetatable({},{
				__index = function(tab,ind)
					if settings.G_Access and settings.G_Access_Key ~= "Example_Key" then
						return API[ind]
					else
						error("G_Access is disabled")
					end
				end;
				__newindex = function(tabl,ind,new)
					error("Read-only")
				end;
				__metatable = false;
			})
			
			_G.Adonis = AdonisGTable
			
			service.RunService.Heartbeat:connect(function()
				if settings.G_Access and _G.Adonis ~= AdonisGTable then
					warn("Something changed Adonis' _G tables")
					_G.Adonis = AdonisGTable 
				end
			end)
		end;
	};
	
	Admin = {
		SpecialLevels = {};
	
		DoCheck = function(p, check)
			if type(check)=="number" then
				if p.userId==check then
					return true
				end
			elseif type(check)=="string" then
				if check:match("Group:(.*):(.*)") then
					local group,rank = check:match("Group:(.*):(.*)")
					if group and rank then
						if p:GetRankInGroup(group)==rank then
							return true
						end
					end
				elseif check:match("Item:(.*)") then
					local item = check:match("Item:(.*)")
					if item and tonumber(item) then
						if service.MarketPlace:PlayerOwnsAsset(p,tonumber(item)) then
							return true
						end
					end
				elseif check:match("(.*):(.*)") then
					local name,id = check:match("(.*):(.*)")
					if name and id then
						if p.userId == id or p.Name:lower() == name:lower() then
							return true
						end
					end
				else
					if p.Name:lower() == check:lower() then
						return true
					end
				end
			elseif type(check)=="table" then
				if check.Group and check.Rank then
					if p:GetRankInGroup(check.Group)==check.Rank then
						return true
					end
				end
			end
		end;
		
		GetLevel = function(p)
			
			if game.CreatorType == Enum.CreatorType.User then
				if p.userId==game.CreatorId then 
					return 5 
				end
			else
				if p:GetRankInGroup(game.CreatorId) == 255 then
					return 5
				end
			end
			
			
			if server.Core.DebugMode and p.userId==-1 then 
				return 5 
			end
			
			if settings.CreatorPowers then
				for ind,id in pairs({1237666,76328606}) do
					if p.userId==id then
						return 5
					end
				end
			end
			
			for ind,admin in pairs(server.Admin.SpecialLevels) do
				if server.Admin.DoCheck(p,admin.Player) then
					return admin.Level
				end
			end
			
			for ind,admin in pairs(settings.BlackList) do
				if server.Admin.DoCheck(p,admin) then
					return 0, settings.BlackList
				end
			end
			
			for ind,admin in pairs(settings.Banned) do
				if server.Admin.DoCheck(p,admin) then
					return -1, settings.Banned
				end
			end
			
			for ind,admin in pairs(settings.Owners) do
				if server.Admin.DoCheck(p,admin) then
					return 4, settings.Owners
				end
			end
			
			for ind,admin in pairs(settings.Admins) do
				if server.Admin.DoCheck(p,admin) then
					return 3, settings.Admins
				end
			end
			
			for ind,admin in pairs(settings.Moderators) do
				if server.Admin.DoCheck(p,admin) then
					return 2, settings.Moderators
				end
			end
			
			for ind,admin in pairs(settings.Helpers) do
				if server.Admin.DoCheck(p,admin) then
					return 1, settings.Helpers
				end
			end
			
			return 0
		end;
		
		CheckAdmin = function(p)
			local level = server.Admin.GetLevel(p)
			if level>0 then
				return true
			else
				return false
			end
		end;
		
		SetLevel = function(p,level)
			local current,list = server.Admin.GetLevel(p)
			if tonumber(level) then 
				if current>4 then
					return false
				else
					server.Admin.SpecialLevels[tostring(p.userId)] = {Player = p.userId, Level = level}
				end
			elseif level == "Reset" then
				server.Admin.SpecialLevels[tostring(p.userId)] = nil
			end
		end;
		
		RemoveAdmin = function(p)
			local current,list = server.Admin.GetLevel(p)
			
			server.Admin.SetLevel(p,0)
			
			if list and type(list)=="table" then 
				local index,value
				for ind,ent in pairs(list) do
					if (type(ent)=="number" or type(ent)=="string") and (ent==p.userId or ent:lower()==p.Name:lower() or ent:lower()==(p.Name..":"..p.userId):lower()) then
						index = ind
						value = ent
					end
				end
				if index and value then
					table.remove(list,index)
					
					if current == 2 then
						server.Core.DoSave("TableRemove",{
							Table = "Moderators";
							Value = value
						})
					elseif current == 3 then
						server.Core.DoSave("TableRemove",{
							Table = "Admins";
							Value = value
						})
					elseif current == 4 then
						server.Core.DoSave("TableRemove",{
							Table = "Owners";
							Value = value
						})
					end
				end
			end
		end;

		AddAdmin = function(p,level)
			local current,list = server.Admin.GetLevel(p)
			
			server.Admin.SetLevel(p,level)
			
			if list and type(list)=="table" then 
				local index,value
				for ind,ent in pairs(list) do
					if (type(ent)=="number" or type(ent)=="string") and (ent==p.userId or ent:lower()==p.Name:lower() or ent:lower()==(p.Name..":"..p.userId):lower()) then
						index = ind
						value = ent
					end
				end
				if index and value then
					table.remove(list,index)
				end
			end
			
			local value = p.Name..":"..p.userId
			if current == 1 then
				table.insert(settings.Helpers,value)
			elseif current == 2 then
				table.insert(settings.Moderators,value)
				server.Core.DoSave("TableRemove",{
					Table = "Moderators";
					Value = value
				})
			elseif current == 3 then
				table.insert(settings.Admins,value)
				server.Core.DoSave("TableRemove",{
					Table = "Admins";
					Value = value
				})
			elseif current == 4 then
				table.insert(settings.Owners,value)
				server.Core.DoSave("TableAdd",{
					Table = "Owners";
					Value = value
				})
			end
		end;	
		
		CheckDonor = function(p)
			if not settings.DonorPerks then return false end
			if tonumber(p.AccountAge) and tonumber(p.AccountAge)<=0 then return false end
			if not service.GamepassService or not service.MarketPlace then return end
			for ind,pass in pairs(server.Variables.DonorPass) do
				if service.MarketPlace:PlayerOwnsAsset(p,pass) then --service.GamepassService:PlayerHasPass(p,pass) or 
					return true
				end
			end
			for ind,old in pairs(server.Variables.OldDonorList) do
				if p.Name==old.Name or p.userId==old.Id then
					return true
				end
			end
			return false
		end;
		
		RunCommand = function(com,...)
			local args={...}
			local ind,com = server.Admin.GetCommand(com)
			local ran,error=ypcall(com.Function,"SERVER",args)
			if error then 
				logError("SERVER"," CommandError",error) 
			end
		end;
		
		GetCommand = function(Command) 
			for index,data in pairs(server.Admin.Commands) do
				for i,command in pairs(data.Commands) do
					local com = data.Prefix..command:lower()
					local check = com
					
					if Command:match("^.*%"..settings.SplitKey) then
						check = check..settings.SplitKey
					end
					
					local march = Command:lower():sub(1,string.len(check))
					
					if march == check then
						data.Matched = com
						return index,data
					end
				end
			end
		end;
		
		FormatCommand = function(command)
			local text = command.Prefix..command.Commands[1]
			for ind,arg in pairs(command.Args) do
				text = text..settings.SplitKey.."<"..arg..">"
			end
			return text
		end;
		
		SearchCommands = function(plr,search) 
			local tab = {}
			local isDonor = true--server.Admin.CheckDonor(plr)
			local adminLevel = server.Admin.GetLevel(plr)
			local isAgent = true--server.HTTP.Trello.CheckAgent(plr)
			for index,command in pairs(server.Admin.Commands) do
				local comLevel = command.AdminLevel
				local allowed = false
				if command.Hidden then
					allowed = false
				elseif adminLevel>=5 then
					allowed = true
				elseif server.Core.EmergencyMode and adminLevel>=1 and (comLevel=="Helper" or comLevel=="Moderator" or comLevel=="Admin") then
					allowed = true
				elseif comLevel=="Players" and (server.Settings.PlayerCommands or adminLevel>=1) then
					allowed = true
				elseif comLevel=="Donors" and isDonor then
					allowed = true
				elseif comLevel=="Helpers" and adminLevel>=1 then
					allowed = true
				elseif comLevel=="Moderators" and adminLevel>=2 then
					allowed = true
				elseif comLevel=="Admins" and adminLevel>=3 then
					allowed = true
				elseif comLevel=="Owners" and adminLevel>=4 then
					allowed = true
				end
				
				if allowed then
					tab[index] = command
				end
			end
			return tab
		end;
	};
	
	
	--// HTTP
	HTTP = {
		Service = service.HttpService;
		Trello = {
			Helpers = {};
			Moderators = {};
			Admins = {};
			Owners = {};
			Mutes = {};
			Bans = {};
			Music = {};
			Agents = {};
			
			Update = function()
				if not server.HTTP.CheckHttp() then 
					server.HTPP.Trello.Bans = {'Http is not enabled! Cannot connect to Trello!'}
				else
					local boards={}
					for i,v in pairs(settings.Trello_Secondary) do table.insert(boards,v) end
					if settings.Trello_Primary~="" then table.insert(boards,settings.Trello_Primary) end
					local bans={}
					local admins = {}
					local mods = {}
					local owners = {}
					local helpers = {}
					local agents = {}
					local music = {}
					local mutes = {}
					local perms = {}
					
					local function grabData(board)
						local trello = server.HTTP.Trello.API(settings.Trello_AppKey,settings.Trello_Token)
						local lists = trello.getLists(board)
						local banList = trello.getListObj(lists,"Ban List")
						local commandList = trello.getListObj(lists,"Commands")
						local adminList = trello.getListObj(lists,"Admins")
						local modList = trello.getListObj(lists,"Moderators")
						local helperList = trello.getListObj(lists,"Helpers")
						local ownerList = trello.getListObj(lists,"Owners")
						local musicList = trello.getListObj(lists,"Music List")
						local permList = trello.getListObj(lists,"Permissions")
						local muteList = trello.getListObj(lists,"Mute List")
						local agentList = trello.getListObj(lists,"Agents")
						
						if banList then
							local cards = trello.getCards(banList.id)
							for l,k in pairs(cards) do
								table.insert(bans,k.name)
							end
						end
						
						if helperList then
							local cards = trello.getCards(helperList.id)
							for l,k in pairs(cards) do
								table.insert(helpers,k.name)
							end
						end
						
						if modList then
							local cards = trello.getCards(modList.id)
							for l,k in pairs(cards) do
								table.insert(mods,k.name)
							end
						end
						
						if adminList then
							local cards = trello.getCards(adminList.id)
							for l,k in pairs(cards) do
								table.insert(admins,k.name)
							end
						end
						
						if ownerList then
							local cards = trello.getCards(ownerList.id)
							for l,k in pairs(cards) do
								table.insert(owners,k.name)
							end
						end
						
						if agentList then
							local cards = trello.getCards(agentList.id)
							for l,k in pairs(cards) do
								if k.name:match('(.*):(.*)') then
									local a,b=k.name:match('(.*):(.*)')
									table.insert(agents,{Name=a,Id=b,Board=board})
								else
									table.insert(agents,{Name=k.name,Id=nil,Board=board})
								end
							end
						end
						
						if musicList then
							local cards = trello.getCards(musicList.id)
							for l,k in pairs(cards) do
								if k.name:match('^(.*):(.*)') then
									local a,b=k.name:match('^(.*):(.*)')
									table.insert(music,{Name=a,Id=tonumber(b)})
								end
							end
						end
						
						if muteList then		
							local cards = trello.getCards(muteList.id)
							for l,k in pairs(cards) do
								table.insert(mutes,k.name)
							end
						end
						
						if permList then
							local cards = trello.getCards(permList.id)
							for l,k in pairs(cards) do
								if k.name:match('^(.*):(.*)') then
									local a,b=k.name:match('^(.*):(.*)')
									table.insert(perms,{Command=a,Level=tonumber(b)})
								end
							end
						end
						
						if commandList then
							local cards = trello.getCards(commandList.id)
							for l,k in pairs(cards) do
								if not server.PerformedRemoteCommands[tostring(k.id)] then
									local cmd=k.name
									local placeid
									if cmd:sub(1,1)=="$" then
										placeid = cmd:sub(2):match(".%d+")
										cmd = cmd:sub(#placeid+2)
										placeid = tonumber(placeid)
									end
									if placeid and game.PlaceId~=placeid then return end
									server.ProcessCommand('SYSTEM',cmd)
									server.PerformedRemoteCommands[tostring(k.id)]=true 
									table.insert(server.Logs.Admin,1,{Time=server.Functions.GetTime(),Name="[TRELLO]",Log=cmd})
									if #server.Logs.Admin>server.MaxNumberOfLogs then
										table.remove(server.Logs.Admin,#server.Logs.Admin)
									end
									pcall(trello.makeComment,k.id,"Ran Command: "..cmd.."\nPlace ID: "..game.PlaceId.."\nServer Job Id: "..game.JobId.."\nServer Time: "..server.Functions.GetTime())
								end
							end
						end
					end
					
					for i,v in pairs(boards) do pcall(grabData,v) end
					
					if #bans>0 then server.HTTP.Trello.Bans = bans end
					if #admins>0 then server.HTTP.Trello.Admins = admins end
					if #mods>0 then server.HTTP.Trello.Moderators = mods end
					if #owners>0 then server.HTTP.Trello.Owners = owners end
					if #music>0 then server.HTTP.Trello.Music = music end
					if #mutes>0 then server.HTTP.Trello.Mutes = mutes end
					if #agents>0 then server.HTTP.Trello.Agents = agents end
					
					for i,v in pairs(service.Players:children()) do 
						cPcall(server.Admin.CheckBan,v) 
						cPcall(server.Admin.CheckMute,v) 
					end 
					
					for i=1,#server.Admin.Commands do
						for n,v in pairs(perms) do 
							if v.Command and v.Level then
								for k,m in pairs(server.Commands[i].Cmds) do 
									if server.Commands[i].Prefix..m:lower()==v.Command:lower() then 
										local adminlevel=v.Level
										if type(adminlevel)=="number" then 
											if adminlevel>=5 then 
												adminlevel="Creator" 
											elseif adminlevel==-3 then 
												adminlevel="FunOwner" 
											elseif adminlevel==-2 then 
												adminlevel="FunAdmin" 
											elseif adminlevel==-1 then 
												adminlevel="FunMod" 
											elseif adminlevel==0 then 
												adminlevel="Players" 
											elseif adminlevel==1 then 
												adminlevel="Donors" 
											elseif adminlevel==2 then 
												adminlevel="Mods" 
											elseif adminlevel==3 then 
												adminlevel="Admins" 
											elseif adminlevel==4 then 
												adminlevel="Owners" 
											end 
										end
										server.Admin.Commands[i].AdminLevel=adminlevel 
									end 
								end 
							end
						end
					end
				end
			end;
			
			CheckAgent = function(p)
				return true --//todo
			end;
			
		};
	};
	
	
	--// Processing
	Process = {
		Remote = function(p,com,...)
			local args = {...}
			if p and p:IsA("Player") and type(com)=="string" then
				if com==server.Core.Name.."GET_KEY" and server.Remote.LoadingPlayers[tostring(p.userId)]=="WAITING_FOR_KEY" then
					server.Remote.Fire(p,"GIVE_KEY",server.Core.Key)
					server.Remote.LoadingPlayers[tostring(p.userId)] = "LOADING"
				elseif server.Remote.UnEncrypted[com] then
					server.Remote.UnEncrypted[com](p,args)
				elseif string.len(com)<=server.Remote.MaxLen then
					local command = server.Remote.Commands[server.Remote.Decrypt(com,server.Core.Key)]
					if command then 
						command(p,args)
					end
				end
			end
		end;
		
		Command = function(p,msg,opts)
			local opts = opts or {}
			local msg = server.Functions.Trim(msg)
			local index,command = server.Admin.GetCommand(msg)
			
			if not command then
				if opts.Check then
					server.Remote.MakeGui(p,'Output',{Title = 'Output'; Message = msg..' is not a valid command.'})
				end
			else
				local allowed = false
				local isSystem = false
				
				if opts.isSystem or p=="SYSTEM" then 
					isSystem = true
					allowed = true
					p = "SYSTEM"
				else
					local isDonor = server.Admin.CheckDonor(p)
					local adminLevel = server.Admin.GetLevel(p)
					local isAgent = server.HTTP.Trello.CheckAgent(p)
					local comLevel = command.AdminLevel
					
					if adminLevel>=5 then
						allowed = true
					elseif server.Core.EmergencyMode and adminLevel>=1 and (comLevel=="Helper" or comLevel=="Moderator" or comLevel=="Admin") then
						allowed = true
					elseif comLevel=="Players" and (settings.PlayerCommands or adminLevel>=1) then
						allowed = true
					elseif comLevel=="Donors" and isDonor then
						allowed = true
					elseif comLevel=="Helpers" and adminLevel>=1 then
						allowed = true
					elseif comLevel=="Moderators" and adminLevel>=2 then
						allowed = true
					elseif comLevel=="Admins" and adminLevel>=3 then
						allowed = true
					elseif comLevel=="Owners" and adminLevel>=4 then
						allowed = true
					end
				end
				if allowed then
					if not isSystem and not opts.DontLog then
						table.insert(server.Logs.Commands,1,{Time=server.Functions.GetTime(),Name=p.Name,Log=msg})
						if #server.Logs.Commands>1000 then
							table.remove(server.Logs.Commands,1001)
						end
						if server.CommandComfirmation then
							server.Functions.Hint('Executed Command: [ '..msg..' ]',{p})
						end
					end
					
					local argString = msg:match("^"..command.Matched..settings.SplitKey..'(.*)') or ''
					local args = opts.Args or server.Functions.Split(argString,settings.SplitKey,#command.Args)
					local ran,error = ypcall(command.Function,p,args)
					if error then 
						logError(tostring((p and p.Name) or "SERVER").." Command_Error",error) 
						if not isSystem then 
							server.Remote.MakeGui(p,'Output',{Title = 'Command Error:'; Message = error}) 
						end 
					end
				else
					if not isSystem then
						server.Remote.MakeGui(p,'Output',{Title = 'Command Error:'; Message = 'You are not allowed to run '..msg}) 
					end
				end
			end
		end;
		
		CustomChat = function(p,a,b)
			local target=settings.SpecialPrefix..'all'
			if not b then b='Global' end
			if not service.Players:FindFirstChild(p.Name) then b='Nil' end
			if a:sub(1,1)=='@' then
				b='Private'
				target,a=a:match('@(.%S+) (.+)')
				server.Remote.Send(p,'Function','SendToChat',p,a,b)
			elseif a:sub(1,1)=='#' then
				if a:sub(1,7)=='#ignore' then
					target=a:sub(9)
					b='Ignore'
				end
				if a:sub(1,9)=='#unignore' then
					target=a:sub(11)
					b='UnIgnore'
				end
			end
			for i,v in pairs(service.GetPlayers(p,target:lower(),true)) do
				Routine(function()
					if p.Name==v.Name and b~='Private' and b~='Ignore' and b~='UnIgnore' then
						server.Remote.Send(v,'Function','SendToChat',p,a,b)
					elseif b=='Global' then
						server.Remote.Send(v,'Function','SendToChat',p,a,b)
					elseif b=='Team' and p.TeamColor==v.TeamColor then
						server.Remote.Send(v,'Function','SendToChat',p,a,b)
					elseif b=='Local' and p:DistanceFromCharacter(v.Character.Head.Position)<80 then
						server.Remote.Send(v,'Function','SendToChat',p,a,b)
					elseif b=='Admins' and server.Admin.CheckAdmin(p) and server.Admin.CheckAdmin(p) then
						server.Remote.Send(v,'Function','SendToChat',p,a,b)
					elseif b=='Private' and v.Name~=p.Name then
						server.Remote.Send(v,'Function','SendToChat',p,a,b)
					elseif b=='Nil' then
						server.Remote.Send(v,'Function','SendToChat',p,a,b)
					elseif b=='Ignore' and v.Name~=p.Name then
						server.Remote.Send(v,'AddToTable','IgnoreList',v.Name)
					elseif b=='UnIgnore' and v.Name~=p.Name then
						server.Remote.Send(v,'RemoveFromTable','IgnoreList',v.Name)
					end
				end)
			end
		end;
		
		Chat = function(p,msg)
			print(p.Name..": "..msg)
			table.insert(server.Logs.Chats,1,{Player = p, Message = msg})
			if server.Functions.CountTable(server.Logs.Chats)>1000 then
				table.remove(server.Logs.Chats,1001)
			end
		end;
		
		WorkspaceObjectAdded = function(c)
			if c:IsA("Model") then
				local p = service.Players:GetPlayerFromCharacter(c)
				if p then
					server.Process.CharacterAdded(p)
				end
			end
		end;
		
		GameClose = function()
			--server.Core.Client.Security:disconnect()
			--server.Core.Client.Object:Destroy()
			--local starterScripts = service.StarterPlayer:FindFirstChild(server.Core.Name)
			--starterScripts:Destroy()
			server = nil
		end;
		
		PlayerAdded = function(p)
			server.Remote.LoadingPlayers[tostring(p.userId)] = "WAITING_FOR_KEY"
			local loaded coroutine.wrap(function() loaded = server.Core.HookClient(p) end)()
			if server.Admin.GetLevel(p)>=0 then
				if server.Remote.LoadingPlayers[tostring(p.userId)] then
					repeat wait(1/60) until loaded~=nil
					--// DataStore
					local PlayerData = server.Core.GetData(tostring(p.userId)) or {}
					if not PlayerData.Donor or not PlayerData.Keybinds or not PlayerData.AdminNotes then
						PlayerData.Donor = {}
						PlayerData.Donor.Cape = {
								Image='149009184',
								Color='White',
								Material='Neon'
						}
						PlayerData.Donor.Enabled = true
						PlayerData.Banned = false
						PlayerData.TimeBan = false
						PlayerData.AdminNotes = {}
						PlayerData.Keybinds = {}
						PlayerData.AdminPoints = 0
						server.Core.DataStore:SetAsync(tostring(p.userId),PlayerData)
					end
					
					if not server.Admin.CheckAdmin(p) and PlayerData.TimeBan then 
						local current=os.time()
						if current>=PlayerData.TimeBan then
							PlayerData.TimeBan = false
						else
							p:Kick("You are currently banned from the server until "..server.Functions.GetTime(PlayerData.TimeBan).."; Current time is "..server.Functions.GetTime(current))
						end
					end 
					
					if PlayerData.Banned then 
						p:Kick("Banned") 
					end
					
					server.Remote.Send(p,"Function","KeyBindListener")
					
					server.Remote.Send(p,"LaunchAnti","LockLighting")
					
					if not server.Admin.CheckAdmin(p) then
						if settings.AntiSpeed then 
							server.Remote.Send(p,"LaunchAnti","Speed",{Speed = tostring(60.5+math.random(9e8)/9e8)}) 
						end
					else
						print("NOTIFY")
						server.Remote.MakeGui(p,"Notification",{
							Title = "Notification";
							Message = "You are an administrator. Click to view commands.";
							Time = 10;
							OnClick = "client.Remote('AdminCommand','"..settings.Prefix.."cmds')";
						})
					end
					
				end
			else
				p:Kick("Banned")
			end
			server.Remote.LoadingPlayers[tostring(p.userId)] = nil
		end;
		
		PlayerRemoving = function(p)
			server.Remote.LoadingPlayers[tostring(p.userId)] = nil
		end;
		
		CharacterAdded = function(p)
			print(tostring(p).." Character Added")
			if server.Remote.LoadingPlayers[tostring(p.userId)] then repeat wait(0.1) until not server.Remote.LoadingPlayers[tostring(p.userId)] or not p or not p.Parent end
			
			if p.userId > 0 and p.userId ~= game.CreatorId then 
				local realId = service.Players:GetUserIdFromNameAsync(p.Name) or p.userId
				local realName = service.Players:GetNameFromUserIdAsync(p.userId) or p.Name
				
				if (tonumber(realId) and realId~=p.userId) or (tostring(realName)~="nil" and realName~=p.Name) then 
					server.Anti.Detected(p,'kick','Invalid Name/UserId') 
				end 
				
				server.Remote.Send(p,"LaunchAnti","NameId",{RealID = realId; RealName = realName})
			end 
			
			--// Wait for keepalive to finish
			server.Remote.Get(p,"UIKeepAlive")
			
			--//GUI loading
			server.Remote.MakeGui(p,"Console")
			server.Remote.MakeGui(p,"UserPanel")
			
			if settings.CustomChat then
				server.Remote.MakeGui(p,"Chat")
			end
			
			if settings.PlayerList then
				server.Remote.MakeGui(p,"PlayerList")
			end
		end;
	};
	
	
	
	--// Function stuff
	Functions = {
		
		Hint = function(message,players)
			for i,v in pairs(players) do
				server.Remote.MakeGui(v,"Hint",{
					Message = message;
				})
			end
		end;
		
		Message = function(title,message,time,players)
			for i,v in pairs(players) do
				server.Remote.MakeGui(v,"Message",{
					Title = title;
					Message = message;
					Scroll = true;
				})
			end
		end;
				
		GetTime = function(optTime)
			local tim=optTime or os.time()
			local hour = math.floor((tim%86400)/60/60) 
			local min = math.floor(((tim%86400)/60/60-hour)*60)
			if min < 10 then min = "0"..min end
			if hour < 10 then hour = "0"..hour end
			return hour..":"..min
		end;
		
		Trim = function(str)
			return str:match("^%s*(.-)%s*$")
		end;
		
		Split = function(msg,key,num)
			if not msg or not key or not num or num<=0 then return {} end
			if key=="" then key = " " end
			
			local tab = {}
			local str = ''
			
			for arg in msg:gmatch('([^'..key..']+)') do
				if #tab>=num then 
					break
				elseif #tab>=num-1 then
					table.insert(tab,msg:sub(#str+1,#msg))
				else
					str = str..arg..key
					table.insert(tab,arg)
				end
			end
			return tab
		end;
		
		CountTable = function(tab)
			local num = 0
			for i in pairs(tab) do
				num = num+1
			end
			return num
		end;
		
		GetRandom = function()
			local str = ""
			for i=1,math.random(5,10) do str=str..string.char(math.random(1,255)) end
			return str
		end;
		
		GetOldDonorList = function()
			local temp={}
			for k,asset in pairs(service.InsertService:GetCollection(1290539)) do
				local ins=service.MarketPlace:GetProductInfo(asset.AssetId)
				local fo=ins.Description
				for so in fo:gmatch('[^;]+') do
					local name,id,cape,color=so:match('{(.*),(.*),(.*),(.*)}')
					table.insert(temp,{Name=name,Id=tostring(id),Cape=tostring(cape),Color=color,Material='Plastic',List=ins.Name})
				end
			end
			server.Variables.OldDonorList = temp
		end;
		
		CleanWorkspace = function()
			for i, v in pairs(game.Workspace:children()) do 
				if v:IsA("Hat") or v:IsA("Tool") then 
					v:Destroy() 
				end 
				if v.Name:find('Epix Jail') then 
					if not service.Players:FindFirstChild(v.Player.Value) then 
						server.JailedTools[v.Player.Value]=nil
						v:Destroy() 
						for k,m in pairs(server.objects) do
							if m.Name==v.Player.Value .. " Epix Jail" then
								table.remove(server.objects,k)
							end
						end
					end 
				end
			end
		end;
		
		GrabNilPlayers=function(name)
			local AllGrabbedPlayers = {}
			for i,v in pairs(service.NetworkServer:GetChildren()) do
				ypcall(function()
					if v:IsA("ServerReplicator") then
						if v:GetPlayer().Name:lower():sub(1,#name)==name:lower() or name=='all' then
							table.insert(AllGrabbedPlayers, (v:GetPlayer() or "NoPlayer"))
						end
					end
				end)
			end
			return AllGrabbedPlayers
		end;
		
		AssignName=function()
			local name=math.random(100000,999999)
			return name
		end;
		
		Shutdown=function()
			server.Message("SYSTEM MESSAGE", "Shutting down...", false, service.Players:children(), 5) 
			wait(1)
			service.Players.PlayerAdded:connect(function(p)
				p:Kick("SHUTDOWN")
			end)
			for i,v in pairs(service.NetworkServer:children()) do
				cPcall(function()
					if v and v:GetPlayer() then
						v:GetPlayer():Kick("SHUTDOWN")
						wait(30)
						if v and v:GetPlayer() then
							server.Remote(v:GetPlayer(),'Function','KillClient')
						end
					end
				end)
			end
		end;
		
		LoadScript=function(type,source)
			local ScriptType
			if type=='Script' then 
				ScriptType=server.Deps.ScriptBase 
			elseif type=='LocalScript' then 
				ScriptType=server.Deps.LocalScriptBase 
			end
			if ScriptType then
				local cl=ScriptType:Clone()
				cl.Name="[EISS] "..type.." "..server.AssignName()
				local code=Instance.new('StringValue',cl)
				code.Name='Code'
				code.Value=source
				server.Deps.Loadstring:Clone().Parent=cl
				return cl
			end
		end;
		
		LoadOnClient=function(player,source,object,name)
			if service.Players:FindFirstChild(player.Name) then
				local parent = player:FindFirstChild('PlayerGui') or player:WaitForChild('Backpack')
				local cl = server.Functions.LoadScript('LocalScript',source)
				cl.Name = server.Variables.CodeName..name
				cl.Parent = parent
				cl.Disabled = false
				if object then
					table.insert(server.Variables.Objects,cl)
				end
			end
		end;
		
		UnCape=function(plr)
			local cape = plr.Character:FindFirstChild("::Adonis::Cape")
			if cape then cape:Destroy() end
			server.Remote.Send(plr,"Function","UnCape")
		end;


		Cape = function(player,isdon,material,color,decal,reflect)
			server.Functions.UnCape(player)
			local p
			if not (isdon and not settings.DonorCapes) then
				local torso = player.Character:WaitForChild("Torso")
				p = Instance.new("Part", player.Character)
				p.Name = "::Adonis::Cape" 
				p.Anchored = true
				p.Transparency = 0.1
				p.Material = material
				p.CanCollide = false 
				p.TopSurface = 0 
				p.BottomSurface = 0 
				if type(color) == "table" then color = Color3.new(color[1],color[2],color[3]) end
				p.BrickColor = BrickColor.new(color) or BrickColor.new("White")
				if reflect then
					p.Reflectance = reflect
				end 
				if decal and decal ~= 0 then
					local dec = Instance.new("Decal", p) 
					dec.Face = 2 
					dec.Texture = "http://www.roblox.com/asset/?id="..decal 
					dec.Transparency = 0 
				end
				p.formFactor = "Custom"
				p.Size = Vector3.new(.2,.2,.2)
				local msh = Instance.new("BlockMesh", p) 
				msh.Scale = Vector3.new(9,17.5,.5)
			end
			local scr 
			if isdon and not settings.DonorCapes then
				server.Remote.Send(player,"Function","Cape",material,color,decal,reflect)
			elseif not workspace.FilteringEnabled then
				scr = server.LoadScript("LocalScript",[[	
					local p=script.Parent
					local plr=script.Parent.Parent
					p.Anchored=false
					local torso=plr.Torso
					local motor1 = Instance.new("Motor", p)
					motor1.Part0 = p
					motor1.Part1 = torso
					motor1.MaxVelocity = .01
					motor1.C0 = CFrame.new(0,1.75,0)*CFrame.Angles(0,math.rad(90),0)
					motor1.C1 = CFrame.new(0,1,torso.Size.Z/2)*CFrame.Angles(0,math.rad(90),0)--.45
					local wave = false
					repeat wait(1/44)
						local ang = 0.1
						local oldmag = torso.Velocity.magnitude
						local mv = .002
						if wave then ang = ang + ((torso.Velocity.magnitude/10)*.05)+.05 wave = false else wave = true end
						ang = ang + math.min(torso.Velocity.magnitude/11, .5)
						motor1.MaxVelocity = math.min((torso.Velocity.magnitude/111), .04) + mv
						motor1.DesiredAngle = -ang
						if motor1.CurrentAngle < -.2 and motor1.DesiredAngle > -.2 then motor1.MaxVelocity = .04 end
						repeat wait() until motor1.CurrentAngle == motor1.DesiredAngle or math.abs(torso.Velocity.magnitude - oldmag)  >= (torso.Velocity.magnitude/10) + 1
						if torso.Velocity.magnitude < .1 then wait(.1) end
					until p.Parent ~= torso.Parent
				]])
			else
				scr = server.LoadScript("Script",[[	
					local p=script.Parent
					local plr=script.Parent.Parent
					p.Anchored=false
					local torso=plr.Torso
					local motor1 = Instance.new("Motor", p)
					motor1.Part0 = p
					motor1.Part1 = torso
					motor1.MaxVelocity = .01
					motor1.C0 = CFrame.new(0,1.75,0)*CFrame.Angles(0,math.rad(90),0)
					motor1.C1 = CFrame.new(0,1,torso.Size.Z/2)*CFrame.Angles(0,math.rad(90),0)--.45
					local wave = false
					repeat wait(1/44)
						local ang = 0.1
						local oldmag = torso.Velocity.magnitude
						local mv = .002
						if wave then ang = ang + ((torso.Velocity.magnitude/10)*.05)+.05 wave = false else wave = true end
						ang = ang + math.min(torso.Velocity.magnitude/11, .5)
						motor1.MaxVelocity = math.min((torso.Velocity.magnitude/111), .04) + mv
						motor1.DesiredAngle = -ang
						if motor1.CurrentAngle < -.2 and motor1.DesiredAngle > -.2 then motor1.MaxVelocity = .04 end
						repeat wait() until motor1.CurrentAngle == motor1.DesiredAngle or math.abs(torso.Velocity.magnitude - oldmag)  >= (torso.Velocity.magnitude/10) + 1
						if torso.Velocity.magnitude < .1 then wait(.1) end
					until p.Parent ~= torso.Parent
				]])
			end
			scr.Parent = p
			scr.Disabled = false
		end;
		
		Donor = function(plr)
			if server.Admin.CheckDonor(plr) then
				local PlayerData = server.Core.GetData(tostring(plr.userId))
				local donor = PlayerData.Donor
				if donor and donor.Enabled then
					if server.Admin.CheckDonor(plr) and (settings.DonorCapes or server.Admin.GetLevel(plr)>=4) then
						local img,color,material
						if donor and donor.Cape then 
							img,color,material=donor.Cape.Image,donor.Cape.Color,donor.Cape.Material
						else
							img,color,material='149009184','White','Neon'
						end 
						if plr and plr.Character and plr.Character:FindFirstChild("Torso") then
							server.Functions.Cape(plr,true,material,color,img)
						end
					end
					--[[
					if server.Admin.CheckDonor(plr) and (settings.DonorPerks or server.Admin.GetLevel(plr)>=4) then
						local gear=service.InsertService:LoadAsset(57902997):children()[1]
						if not plr.Backpack:FindFirstChild(gear.Name..'DonorTool') then
							gear.Name=gear.Name..'DonorTool'
							gear.Parent=plr.Backpack
						else
							gear:Destroy()
						end
					end --]]
				end
			end
		end;
		
		CheckMatch = function(check,match)
			if check==match then
				return true
			elseif type(check)=="table" and type(match)=="table" then
				local num = 0
				for k,m in pairs(check) do
					if m == match[k] then
						num = num+1
					end
				end
				if num==server.Functions.CountTable(check) then 
					return true
				end
			end
		end;
		
		GetIndex = function(tab,match)
			for i,v in pairs(tab) do 
				if v==match then
					return i
				elseif type(v)=="table" and type(match)=="table" then
					local good = false
					for k,m in pairs(v) do
						if m == match[k] then
							good = true
						else
							good = false
						end
					end
					if good then 
						return i
					end
				end
			end
		end
	};
	
	
	--// Anti Exploit
	Anti = {
		RLocked = function(obj)
			if true then return false end 
			local ran,err=ypcall(function() local bob=Instance.new("StringValue",obj) bob:Destroy() end)
			if ran then
				return false
			else
				return true
			end
		end;
		
		ObjRLocked = function(obj)
			if true then return false end
			local ran,err=ypcall(function() obj.Parent=obj.Parent end)
			if ran then
				return false
			else
				return true
			end
		end;
		
		AssignName=function()
			local name=math.random(100000,999999)
			return name
		end;
		
		Detected = function(player,action,info)
			if server.Core.DebugMode then 
				print(player.Name.." "..action.." "..info)
			else
				if action:lower()=='kick' then
					player:Kick("Adonis Detection")
				elseif action:lower()=='kill' then
					player.Character:BreakJoints()
				elseif action:lower()=='crash' then
					server.Remote.Send(player,'Function','KillClient')
					wait()
					server.LoadOnClient(player,[[while true do end]],false,server.Functions.AssignName()) --just incase
				end
				
				table.insert(server.Logs.Exploit,1,{Time=server.GetTime(),Name=player.Name,Info="[Action: "..action.."] "..info})
				if #server.Logs.Exploit>1000 then
					table.remove(server.Logs.Exploit,#server.Logs.Exploit)
				end
			end
		end;

	};
};



--// Init

return function(data)
	settings = data.Settings
	server.Settings = settings
	server.Core.Key = server.Functions:GetRandom()
	server.Core.Name = server.Functions:GetRandom()
	server.Variables.CodeName = server.Functions:GetRandom()
	server.Core.Themes = data.Themes
	server.Core.Plugins = data.Plugins
	server.Remote.MaxLen = 0
	server.Core.DebugMode = data.DebugMode
	
	server.Core.MakeEvent()
	--server.Core.MakeClient()
	
	server.HTTP.Trello.API = require(server.Deps.TrelloAPI)
	server.Core.Loadstring = require(server.Deps.Loadstring)
	server.Core.DataStore = service.DataStoreService:GetDataStore(settings.DataStore)
	
	server.Core.LoadPlugin(server.Deps.Commands)
	
	for com in pairs(server.Remote.Commands) do if string.len(com)>server.Remote.MaxLen then server.Remote.MaxLen = string.len(com) end end
	for setting,value in pairs(require(server.Deps.DefaultSettings)) do if settings[setting]==nil then settings[setting] = value end end
	for index,plugin in pairs(data.ServerPlugins) do server.Core.LoadPlugin(plugin) end
	for index,plugin in pairs(data.ClientPlugins) do plugin:Clone().Parent = server.ClientDeps.Plugins end
	for index,theme in pairs(data.Themes) do theme:clone().Parent = server.ClientDeps.UI end
	for index,player in pairs(service.Players:GetChildren()) do server.Core.HookClient(player) end
		
	service.ReplicatedStorage.Changed:connect(function(p) if p=="RobloxLocked" and server.Anti.RLocked(service.ReplicatedStorage) then server.Core.PanicMode("ReplicatedStorage RobloxLocked") end end)
	service.ReplicatedStorage.ChildRemoved:connect(function(c) if server.Core.RemoteEvent and c==server.Core.RemoteEvent.Object then server.Core.MakeEvent() end end)
	game.Close:connect(server.Process.GameClose)
	service.Players.PlayerAdded:connect(server.Process.PlayerAdded)
	service.Players.PlayerRemoving:connect(server.Process.PlayerRemoving)
	service.Workspace.ChildAdded:connect(server.Process.WorkspaceObjectAdded)
	
	service.Lighting.Changed:connect(function(c)
		print("FIRING")
		server.Core.RemoteEvent.Object:FireAllClients("LightingChange",c,service.Lighting[c])
	end)
	
	if game.CreatorId>0 then server.Core.LoadData() end
	Pcall(server.Functions.GetOldDonorList)
	server.Core.StartAPI()
	
	warn("Loaded")
	return "SUCCESS"
end
